// GLOBALS ---------------------------------------------------------------------

var data_arr = [
    [
        {naam:"de fruitmand",adres:"steenstraat 34", post:8000,gemeente:"Brugge",tel:"050342218",manager:"Francine Lapoule"},
        {naam:"Jos & Anneke",adres:"visserijstraat 1", post:8400,gemeente:"Oostende",tel:"059463689",manager:"Jos Leman"},
        {naam:"groene vingers",adres:"hoogstraat 108", post:9000,gemeente:"Gent",tel:"091342218"},
        {naam:"de buurtwinkel",adres:"die laene 22", post:2000,gemeente:"Antwerpen",tel:"0230342218",manager:"Bert Simoens"}
    ],
    [
        ["aardappelen",0.95,"kg"],
        ["avocado",2.69,"stuk"],
        ["bloemkool",1.93,"stuk"],
        ["brocoli",1.29,"stuk"],
        ["champignons",0.89,"250g"],
        ["chinese kool",1.59,"stuk"],
        ["groene kool",1.69,"stuk"],
        ["knolselder",1.29,"stuk"],
        ["komkommer",2.49,"stuk"],
        ["kropsla",1.69,"stuk"],
        ["paprika",0.89,"net"],
        ["prei",2.99,"bundel"],
        ["princessenbonen",1,"250g"],
        ["rapen",0.99,"bundel"],
        ["kropsla",1.69,"stuk"],
        ["rode kool",1.39,"stuk"],
        ["sla iceberg",1.49,"stuk"],
        ["spinazie vers",1.89,"300g"],
        ["sjalot",0.99,"500g"],
        ["spruiten",1.86,"kg"],
        ["trostomaat",2.99,"500g"],
        ["ui",0.89,"kg"],
        ["witloof 1ste keus",1.49,"700g"],
        ["wortelen",2.59,"kg"],
        ["courgetten",1.5,"stuk"]
    ]
];

// WINDOW.ONLOAD ---------------------------------------------------------------

window.onload = function() {
    // find both select elements
    var select_elm = [
        document.frmBestel.winkel,
        document.frmBestel.groente
    ];
    // *** DEBUG *** console.log(select_elm[0]);
    // *** DEBUG *** console.log(select_elm[1]);
    
    // create option elements for shop and vegetable select elements
    for (var i=0; i<2; i++) {
        createSelectOptions(select_elm[i], data_arr[i]);
    }
    
    // event handler for 'add to shopping' cart button
    var addToCartButton_elm = document.getElementById('toevoegen');
    addToCartButton_elm.addEventListener('click', function() {
        var winkel_str =    document.getElementById('winkel').value;
        var groente_str =   document.getElementById('groente').value;
        var aantal_str =    document.getElementById('aantal').value;
        var valid_str = validateInput(winkel_str, groente_str, aantal_str);
        // *** DEBUG *** console.log('return value validateInput(): ', valid_str); // *** DEBUG *** 
        if (valid_str == 'ok') { // input processed only if validated
            processInput(groente_str, aantal_str);
        } else {
            // relevant warning message depending on return value of function validateInput
            switch (valid_str) {
                case 'winkel':
                    alert ('U heeft geen geldige winkel geslecteerd!');
                    document.getElementById('winkel').focus();
                    break;
                case 'groente':
                    alert ('U heeft geen geldige groente geslecteerd!');
                    document.getElementById('groente').focus();
                    break;
                case 'aantal':
                    alert ('U heeft geen geldig aantal geslecteerd!');
                    document.getElementById('aantal').focus();                    
                    break;
            }
        }
    });
};

// FUNCTIONS & EVENT HANDLERS --------------------------------------------------

function createSelectOptions(select_elm, dataSet_arr) {
    /* fills out the option elements in a given select
     * @selectId_str    {string}:   name of the select element
     * @dataSet_arr     {array}:    data from which the options are constructed
     * returns:         {string}:   the string containing all option elements
     */

    // *** DEBUG *** console.log('select element name= ', select_elm.name); // *** DEBUG *** 
    // *** DEBUG *** console.log('dataset= ', dataSet_arr); // *** DEBUG *** 
    // *** DEBUG *** console.log('dataset lenght= ', dataSet_arr.length); // *** DEBUG *** 
    
    var option_elm, option_str, value, title, text;
    for (i=0; i<dataSet_arr.length; i++) {
        if (select_elm.name == 'winkel') {
            value = dataSet_arr[i].naam;
            text = value;
            title = dataSet_arr[i].adres + ', ' + dataSet_arr[i].post + ' ' + dataSet_arr[i].gemeente;
        } else if (select_elm.name == 'groente') {
            value = dataSet_arr[i][0];
            text = value + ' (' + dataSet_arr[i][1] + ' €/' + dataSet_arr[i][2] + ')';
            title = 'undefined';
        }
        option_elm = document.createElement('option');
        option_elm.value = value;
        option_elm.innerHTML = text;
        if (title != 'undefined') {
            option_elm.title = title;
        };
        select_elm.appendChild(option_elm);
    }
    // *** DEBUG *** console.log(select_elm); // *** DEBUG *** 

}

function validateInput(winkel, groente, aantal) {
    // alert('input validation is called!');
    // *** DEBUG *** console.log('typeof aantal = ', typeof aantal, aantal); // *** DEBUG ***
    aantal_num = parseInt(aantal);
    // *** DEBUG *** console.log('typeof aantal_num = ', typeof aantal_num, aantal_num); // *** DEBUG ***
    if (winkel == '') {
        // *** DEBUG *** console.log ('winkel =', winkel); // *** DEBUG *** 
        return 'winkel';
    } else if (groente == '') {
        // *** DEBUG *** console.log ('groente =', groente); // *** DEBUG ***   
        return 'groente';
    } else if (aantal_num <= 0 || isNaN(aantal_num) ) {
        // *** DEBUG *** console.log ('aantal =', aantal); // *** DEBUG *** 
        return 'aantal';
    } else {
        return 'ok';
    }
}

function processInput(product, aantal) {
    // *** DEBUG *** alert('input is being processed'); // *** DEBUG *** 
    shoppingCart_elm = document.getElementById('winkelmandje');
    var emptyLine_elm = document.querySelector('#leeg');
    var unitPrice_num = (function() {
        for (var i=0; i<data_arr[1].length; i++) {
            if (data_arr[1][i][0] == product) {
                return data_arr[1][i][1];
            }
        }
    return -1;
    })();
    console.log('unitPrice_num = ', unitPrice_num);
    var shoppingCartLine_elm, line_elm, product_elm, amount_elm, unitPrice_elm, subTotal_elm;
    emptyLine_elm.style.display = 'none';
    if (document.getElementById(product)) {
        // add 'aantal' and recalculate subtotal for existing line
        existingLine_elm = document.getElementById(product);
        var subTotal_num = parseFloat(existingLine_elm.querySelector('.celrechts').innerHTML);
        var totalPrice_elm = document.getElementById('totNum');
        var totalPrice_num = parseFloat(totalPrice_elm.innerHTML);
        totalPrice_num -= subTotal_num;
        subTotal_num += (aantal * unitPrice_num);
        existingLine_elm.querySelector('.celrechts').innerHTML = subTotal_num.toFixed(2);
        var amount_elm = existingLine_elm.querySelector('.cellinks div:nth-child(2)');
        var amount_num = parseInt(amount_elm.innerHTML);
        // *** DEBUG *** console.log('amount = ', amount_num); // *** DEBUG *** 
        amount_num += Number(aantal);
        // *** DEBUG *** console.log('amount = ', amount_num); // *** DEBUG ***         
        amount_elm.innerHTML = amount_num;
        totalPrice_num += subTotal_num;
        totalPrice_elm.innerHTML = totalPrice_num.toFixed(2);
    } else {
        // div . full shopping cart line
        shoppingCartLine_elm = document.createElement('div');
        shoppingCartLine_elm.classList.add('item');
        shoppingCartLine_elm.setAttribute('id', product); 
        // *** DEBUG *** console.log('shoppingCartLine_elm = ', shoppingCartLine_elm); // *** DEBUG *** 
            
            // div . left side of the line (product, amount, unitprice)
            line_elm = document.createElement('div');
            line_elm.classList.add('cellinks');
            line_elm.style.width = '400px';
            // *** DEBUG *** console.log('line_elm = ', line_elm); // *** DEBUG *** 
            
                // div . product
                product_elm = document.createElement('div');
                product_elm.classList.add('cel');
                product_elm.innerHTML = product;
                product_elm.style.width = '196px';
                // *** DEBUG *** console.log('product_elm = ', product_elm); // *** DEBUG *** 

                // div . amount
                amount_elm = document.createElement('div');
                amount_elm.classList.add('cel');
                amount_elm.innerHTML = aantal;
                amount_elm.style.width = '96px';
                amount_elm.style.textAlign = 'right';
                // *** DEBUG *** console.log('amount_elm = ', amount_elm); // *** DEBUG *** 

                // div . unitPrice
                unitPrice_elm = document.createElement('div');
                unitPrice_elm.classList.add('cel');
                unitPrice_elm.innerHTML = unitPrice_num;
                unitPrice_elm.style.width = '96px';
                unitPrice_elm.style.textAlign = 'right';                
                // *** DEBUG *** console.log('unitPrice_elm = ', unitPrice_elm); // *** DEBUG *** 

            // div . subTotal_elm
            subTotal_elm = document.createElement('div');
            subTotal_elm.classList.add('celrechts');
            var amount_num = parseFloat(aantal);
            var subTotal_num = parseFloat(amount_num * unitPrice_num);
            subTotal_elm.innerHTML = subTotal_num.toFixed(2);
            subTotal_elm.style.width = '100px';
            subTotal_elm.style.backgroundColor = 'rgb(175, 175, 175)';
            // *** DEBUG *** console.log('subTotal_elm = ', subTotal_elm); // *** DEBUG *** 
        
        line_elm.appendChild(product_elm);
        line_elm.appendChild(amount_elm);
        line_elm.appendChild(unitPrice_elm);        
        shoppingCartLine_elm.appendChild(line_elm);
        shoppingCartLine_elm.appendChild(subTotal_elm);
        shoppingCart_elm.insertBefore(shoppingCartLine_elm, emptyLine_elm);
        
        // adjust total price
        var totalPrice_elm = document.getElementById('totNum');
        var totalPrice_num = parseFloat(totalPrice_elm.innerHTML);
        totalPrice_num += subTotal_num;
        totalPrice_elm.innerHTML = totalPrice_num.toFixed(2);
    };   
}