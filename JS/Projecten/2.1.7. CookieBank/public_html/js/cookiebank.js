window.onload = function() {
    // DOM elements
    var output_elm =        document.querySelector('#output');
    var creditButton_elm =  document.querySelector('#krediet');
    var debetButton_elm =   document.querySelector('#debiet');
    
    // default values
    var msg_str =       '';                 // message to user
    var name_str =      'nieuwe klant';     // default name content
    var balance_num =   0;                  // default balance
    
    // test cookie presence
    if (getCookie('klantnaam')) {
        // known client
        var name_str =      getCookie('klantnaam');
        var balance_num =   parseFloat(getCookie('saldo')).toFixed(2);
        
        // output message
        msg_str += 'Welkom ' + name_str + ', ';
        msg_str += 'uw saldo bedraagt ' + balance_num + ' euro';
        
        // button
        var button_elm = createButton('Sluit rekening');
        button_elm.addEventListener('click', closeAccount); // event handler
    } else {
        // first time visitor
        msg_str += 'Welkom beste bezoeker. ';
        msg_str += 'Als u bij ons een nieuwe rekening opent, ';
        msg_str += 'ontvangt u een startsaldo van 100 euro!';
        
        // button
        var button_elm = createButton('Open rekening');
        button_elm.addEventListener('click', openAccount); // event handler
    }
    
    // generic DOM elements
    var msg_df =    document.createDocumentFragment();
    var n1_elm =        document.createElement('br');
    
    // complete documentFragment and insert into DOM
    var node_txt =      document.createTextNode(msg_str);
    msg_df.appendChild(node_txt);
    msg_df.appendChild(n1_elm.cloneNode(false));
    msg_df.appendChild(n1_elm.cloneNode(false));
    msg_df.appendChild(button_elm);
    output_elm.appendChild(msg_df);
    
    // event halndler for +/- buttons
    creditButton_elm.addEventListener('click',
        function() {
            calculate('+');
        });
    debetButton_elm.addEventListener('click',
        function() {
           calculate('-'); 
        });
};

//--------------------------- functions ----------------------------------------

function createButton(text) {
    // returns a DOM button element
    var button_elm =    document.createElement('button');
    var text_elm =      document.createTextNode(text);
    button_elm.appendChild(text_elm);
    button_elm.setAttribute('type', 'button');
    return button_elm;
}

function openAccount() {
    // console.log('rekening openen');
    var name_str = window.prompt('Uw naam, graag?', '');
    if (name_str != '' && name_str != null) {
        setCookie('klantnaam', name_str, 100);
        setCookie('saldo', 100, 100);
        window.history.go(0);
    }
}

function closeAccount() {
    // console.log('rekening sluiten');
    // destroys the account, thus also the cookies
    clearCookie('klantnaam');
    clearCookie('saldo');
    window.history.go(0);
}

function showWarning(msg) {
    /* shows a warning text in div.waarschuwing
     * 
     * @msg {string}: the warning text to be shown
     * @returns {undefined}
     */
    
    var warning_elm = document.querySelector('div.waarschuwing');
    warning_elm.innerHTML = msg;
    warning_elm.style.display = 'block';
}

// event handlers

function calculate(operator) {
    /* add to or remove from your account an amount from input element 'bedrag'
     * @operator {string}: '+' or '-' sign
     */
    
    var newBalance_num =    0;
    var amount_elm =        document.getElementById('bedrag');
    var amount_str =        amount_elm.value;
    var balance_str =       getCookie('saldo');
    var message_str =       '';
    
    // replace decimal comma's with decimal points
    var re =                /,/;
    amount_str = amount_str.replace(re, '.');
    
    if (balance_str != null && balance_str != '') {
        if (amount_str != '' && !isNaN(amount_str)) {
            balance_num =   parseFloat(balance_str);
            amount_num =    parseFloat(amount_str);
            switch (operator) {
                case '+':
                    newBalance_num = balance_num + amount_num;
                    break;
                case '-':
                    newBalance_num = balance_num - amount_num;
                    break;
            }
            if (newBalance_num <= 0) {
                var max_num = balance_num - 1;
                message_str += 'Uw saldo is onvoldoende om dit bedrag af te halen. ';
                message_str += 'U kunt maximaal ' + max_num + ' euro afhalen.';
                amount_elm.value = max_num;
                amount_elm.focus();
                showWarning(message_str);
            } else {
            setCookie('saldo', newBalance_num, 100);
            window.history.go(0);
            amount_elm.value = '';
            }
        } else {
            alert('U moet een correct bedrag ingeven!');
        }
    } else {
        // no balance = no account
        var open_boo = window.confirm('U heeft nog geen rekening geopend. Nu even doen?');
        if (open_boo) {
            openAccount();
        }
    }
}