var featuresList = [
    'document.images',
    'document.layers',
    'document.all',
    'document.getElementById',
    'document.querySelector',
    'document.styleSheets',
    'document.createElement',
    'document.createNodeIterator',
    'document.implementation.createDocument',
    'window.walkTheDog',
    'window.focus',
    'window.ActiveXObject',    
   'window.XMLHttpRequest',
    'window.localStorage', 
    '[].push',  
    '[].filter', 
    'Object.prototype', 
    'navigator.geolocation', 
    'document.documentElement.classList'     
];

var browserInfo = [
    'navigator.appName',
    'navigator.appCodeName',
    'navigator.appVersion',
    'navigator.product',
    'navigator.userAgent',
    'navigator.platform',
    'navigator.javaEnabled()'
];

window.onload = function() {
    // display browser info
    var browserInfo_elem = document.getElementById('browserInfo');
    for (var i=0; typeof browserInfo[i] !== 'undefined'; i++) {    
        var pee = browserInfo_elem.appendChild(document.createElement('p'));
        pee.innerHTML = browserInfo[i].replace('navigator.', '') + ': ' + eval(browserInfo[i]);
    }
    // fill table body with features test results
    var tabelBody_elem = document.getElementById('featureTable');
    console.log(tabelBody_elem);
    for (var i=0; typeof featuresList[i] !== 'undefined'; i++) {
        var rij_elem = document.createElement('tr');
        var cel_elem = document.createElement('td');
        featureCel_elem = rij_elem.appendChild(cel_elem);
        featureCel_elem.innerHTML = featuresList[i];
        cel_elem = document.createElement('td');
        supportedCel_elem = rij_elem.appendChild(cel_elem);
        supportedCel_elem.innerHTML = eval(featuresList[i])? 'true' : 'false';
        supportedCel_elem.style.backgroundColor = eval(supportedCel_elem.innerHTML)? 'green' : 'red';
        featureCel_elem.style.backgroundColor = eval(supportedCel_elem.innerHTML)? 'green' : 'red';
        tabelBody_elem.appendChild(rij_elem);
    }
};
