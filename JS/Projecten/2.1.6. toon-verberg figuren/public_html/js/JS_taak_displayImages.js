var HideAllButton = {
    nl: 'Alle schermen verbergen',
    en: 'Hide all screenshots',
    fr: "Masquer toutes les captures d'écran",
    it: 'Nascondere tutte le immagini',
    es: 'ocultar todas las capturas',
    de: 'Verstecken Alle Screenshots'
};
var HideThisButton = {
    nl: 'Dit scherm verbergen',
    en: 'Hide this screenshot',
    fr: "Cacher cette capture d'écran",
    it: 'Nascondere questo screenshot',
    es: 'ocultar esta pantalla',
    de: 'Verstecken diesen Screenshot'    
};
var ShowAllButton = {
    nl: 'Alle schermen tonen',
    en: 'Show all screenshots',
    fr: "Afficher toutes les captures d'écran",
    it: 'mostrare tutte le immagini',
    es: 'mostrar todas las capturas',
    de: 'Alle Screenshots anzeigen'    
};
var ShowThisButton = {
    nl: 'Dit scherm tonen',
    en: 'Show this screenshot',
    fr: "Afficher cette captures d'écran",
    it: 'Mostra questa schermata',
    es: 'Mostrar esta pantalla',
    de: 'Zeigen diesen Bildschirm'    
};
    
window.onload = function() {
    /* set default classname and event listener for hoofdknop */
    var language = document.documentElement.lang;
    /* DEBUG */ console.log('window.onload language: ', language);
    var hoofdKnop_elem = document.querySelector('#hoofdknop');
    /* DEBUG */ console.log('window.onload hoofdKnop: ', hoofdKnop_elem);    
    /* DEBUG */ console.log('window.onload hoofdKnop klasse: ', hoofdKnop_elem.className);    
    if (hoofdKnop_elem.className === '') {
        hoofdKnop_elem.className = 'hide';
    }
    if (hoofdKnop_elem.className === 'hide') {
        hoofdKnop_elem.innerHTML = HideAllButton[language];
    } else {
        hoofdKnop_elem.innerHTML = ShowAllButton[language];
    }
    hoofdKnop_elem.addEventListener('click', toggleAllImagesDisplay);
    
    /* set buttons, button language and event listeners for individual screenshots */
    var screenshots = document.querySelectorAll('.screenshot');
    var knop, toggleKnop;
    for (var i=0; i<screenshots.length; i++) {
        knop = document.createElement('button');
        var knopTekst;
        /* DEBUG */ console.log('window.onload screenshots[i]: ', screenshots[i]);   
        /* DEBUG */ console.log('window.onload screenshots[i].style.display: ', screenshots[i].style.display); 
        if (screenshots[i].style.display === '') {
            screenshots[i].style.display = 'block';
        }
        if (screenshots[i].style.display === 'block') {
            knopTekst = document.createTextNode(HideThisButton[language]);
        } else {
            knopTekst = document.createTextNode(ShowThisButton[language]);
        }
        knop.appendChild(knopTekst);
        toggleKnop = screenshots[i].parentNode.appendChild(knop);
        toggleKnop.addEventListener('click', toggleThisImageDisplay);
    }
};

function toggleAllImagesDisplay() {
    var language = document.documentElement.lang;
    /* DEBUG */ console.log('toggleAllImagesDisplay language: ', language);
    var screens = document.querySelectorAll('.toggle img');
    var DisplayType;
    if (this.className === 'hide') {
        DisplayType = 'none';
        this.className = 'show';
        this.innerHTML = ShowAllButton[language];
    } else {
        DisplayType = 'block';
        this.className = 'hide';
        this.innerHTML = HideAllButton[language];        
    }
    for (var i=0; i<screens.length; i++) {
        screens[i].style.display = DisplayType;
        var knop = screens[i].parentNode.querySelector('button');
        /* DEBUG */ console.log('toggleAllImagesDisplay DisplayType', DisplayType);
        if (DisplayType === 'none') {
            knop.innerHTML = ShowThisButton[language]; 
        } else {
            knop.innerHTML = HideThisButton[language];            
        }
        /* DEBUG */ console.log('toggleAllImagesDisplay knop', knop);
    }
}

function toggleThisImageDisplay() {
    var language = document.getElementsByTagName('html')[0].getAttribute('lang');
    var image = this.parentNode.querySelector('.screenshot');
    /* DEBUG */ console.log(image);
    if (image.style.display === 'none') {
        image.style.display = 'block';
        this.innerHTML = HideThisButton[language];
    } else {
        image.style.display = 'none';
        this.innerHTML = ShowThisButton[language];
    }
}