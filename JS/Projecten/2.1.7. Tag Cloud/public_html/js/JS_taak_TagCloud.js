//het array met de tags
//elk item is zelf een Array met de naam, het huidig aantal stemmen en het vorig aantal stemmen die het kreeg
var arrTags = [
				['Javascript', 1634, 987],
				['jQuery', 1111, 34],
				['PHP', 1024,1122],
				['Asp.Net', 977, 1005],
				['Photoshop', 594, 789],
				['XML', 40, 666],
				['Access', 55, 77],
				['Java', 278, 277],
				['MySQL', 155, 122]
			   ];

window.onload = function() {
    var tagContainer = document.querySelector('#tagContainer');
    tagContainer.style.textAlign = 'center';
    var text, size, x, y, color, dataElement, dataContent;
    for (var i = 0; i<arrTags.length; i++) {
        text = arrTags[i][0];
        size = Math.max(1, Math.round(arrTags[i][1] / 10));
        console.log(size);
        x = Math.round(Math.random() * 500);
        y = Math.round(Math.random() * 400);
        if (arrTags[i][1] > arrTags[i][2]) {
            color = 'green';
        } else if (arrTags[i][1] < arrTags[i][2]) {
            color = 'red';
        }
        dataElement = document.createElement('span');
        dataElement.setAttribute('class', 'tag');
        dataElement.style.position = 'absolute';
        dataElement.style.fontSize = size + 'px';
        dataElement.style.color = color;
        dataElement.style.left = x + 'px';
        dataElement.style.top = y + 'px';
        dataContent = document.createTextNode(text);
        dataElement.appendChild(dataContent);
        tagContainer.appendChild(dataElement);
    }
};