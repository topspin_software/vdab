window.onload = function() {
    // DOM elements
    var divOutput =     document.querySelector('#output');
    var divKalender =   document.querySelector('#kalender');
    
    // leeg inhoud
    emptyNode(divOutput); // function from js/nuttig_lib.js
    
    // put current date and time in output element
    var text = document.createTextNode(getTodayString());
    divOutput.appendChild(text);
    
    // highlight special days
    divKalender.innerHTML = createYearTable(thisYear);
    // today
    highlightDay(today, 'vandaag'); // huidige datum aanduiden
    // birthday
    var birthday = new Date(thisYear, 06, 18);
    highlightDay(birthday, 'verjaardag');
};

function createMonthTable(calenderYear, monthIndex) {
    /* dependency: nuttig_lib.js
     * return: string, for innerHTML: a table with a month overview
     * @calenderYear : integer, 4 digit year
     * @month_index: integer, range 0 (januari) - 11 (december) */
    
    // argument validation
    if (isNaN(calenderYear) || (calenderYear.toString().length !== 4)) {
        return 'foutief jaargetal!';
    }
    if (isNaN(monthIndex) || (monthIndex < 0) || (monthIndex > 11)) {
        return 'foutief maandgetal!';
    }
    
    // fisrt day of the month weekday
    var startDate =     new Date(calenderYear, monthIndex, 1);
    var startWeekday =  startDate.getDay();
    
    // calculate enddate for this month,
    // with possible expception for february in a leap year
    var endDay = months_arr[monthIndex][1];
    if ((monthIndex === 1) && (isThisALeapYear(calenderYear))) {
        endDay = 29;
    }
    
    // building the string return value
    var monthTable_str = '<table class="kalender">\n';
    // title row
    monthTable_str += '<tr><th colspan = "7">' + months_arr[monthIndex][0] + ' ';
    monthTable_str += calenderYear + '<th><tr>\n';
    
    // day titles
    monthTable_str += '<tr>';
    for (var i=0; i<7; i++) {
        monthTable_str += '<td>' + weekdays_arr[i].substr(0,2).toUpperCase() + '</td>';
    }
    monthTable_str += '</tr>\n';
    var day = 1;
    var teller = 0;
    while (day <= endDay) {
        // weekly row
        monthTable_str += '<tr>';
        for (var i=0; i<7; i++) {
            // draw cells, with or without day filled out
            var id_str = ''; //id composed of monthIndex and dayIndex
            var dayIndex_str = ''; // dayIndex
            // write days
            if ((teller >= startWeekday) && (day<= endDay)) {
                dayIndex_str = day;
                id_str = ' id="' + calenderYear + '_' + monthIndex + '_' + dayIndex_str + '"';
                day++;
            }
            // write cell
            monthTable_str += '<td ' + id_str + '>' + dayIndex_str + '</td>';
            teller++;
        }
        monthTable_str += '</tr>\n';
    }
    monthTable_str += '</table>\n';
    return monthTable_str;
}

function createYearTable(calenderYear) {
    /* dependency: createMonthTable()
     * return: string, for innerHTML: 12 monthly tables
     * @calenderYear {integer} 4 digit year
     */
    
    var yearCalender_str = '';
    for (i=0; i<12; i++) {
        yearCalender_str += '<div class="maandContainer">';
        yearCalender_str += createMonthTable(calenderYear, i);
        yearCalender_str += '</div>';
    }
    return yearCalender_str;
}

function highlightDay(date_obj, cssClass_str) {
    /* required:        CSS class in stylesheet
     *                  id in element
     * @date_obj:       date object of the tag thats needs to be highlighted
     * @cssClass_str:   name of css class
     */
    
    // what year, month and day?
    var day =       date_obj.getDate();
    var month =     date_obj.getMonth();
    var year =      date_obj.getFullYear();
    
    // construct cell id
    var id_str = year + '_' + month + '_' + day;
    var cell_elm = document.getElementById(id_str);
    if (cell_elm) {
        cell_elm.className = cssClass_str;
    }    
}