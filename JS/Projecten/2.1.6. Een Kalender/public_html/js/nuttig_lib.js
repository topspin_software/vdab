// javascript library

/************************* DOM functions ***************************************/

function emptyNode(objNode) {
    /*  removes all content/children of a particular node
        @ objNode: node, mandatory, the node that is to be emptied
    */
    while (objNode.hasChildNodes()) {
        objNode.removeChild(objNode.firstChild);
    }
}

/************************ DATE & TIME functions *******************************/

// global DATE objects
var today =         new Date();
var thisDay =       today.getDate(); // day of the month
var thisWeekday =   today.getDay();  // current weekday
var thisMonth =     today.getMonth();
var thisYear =      today.getFullYear(); 

// DATE arrays
// days in getDay() order
var weekdays_arr = new Array('zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag');
// replace feb days for a leap-year
var months_arr = new Array(['januari', 31], ['februari', 28], ['maart', 31], ['april', 30], ['mei', 31], ['juni', 30], ['juli', 31], ['augustus', 31], ['september', 30], ['oktober', 31], ['november', 30], ['december', 31]);

function getTodayString () {
    // returns a datestring in local time
    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    var StrNow = 'Momenteel: ';
    StrNow += today.toLocaleDateString('nl-be', options) + ', ';
    StrNow += today.toLocaleTimeString('nl-be');
    return StrNow;
}

function isThisALeapYear(year) {
    /* tests whether a given year is a leap year.
     * @year: mandatory, number
     * Returns true if it is. */
    var result = false;
    if (!isNaN(year)) {
        if (year % 4 === 0) {
            result = true;
            if (year % 100 === 0) {
                result = false;
                if (year % 400 === 0) {
                    result = true;
                }
            }
        }
    }
    return result;
}
