//============== GLOBALS =======================================================
//array van functies
var aFuncties	= ['instructeur','bediende','manager','arbeider'];
//array van personen, elke persoon is een object
var aoPersoneel = [
	{id:4678,naam:'Roger Mary', functie:'instructeur', leeftijd: 65, sexe:'m', gehuwd:true, 
        kinderen:[{naam:'Liesbeth', leeftijd: 26, sexe:'v'}],vrienden:24},
	{naam:'Evelyn Van Welsenaers', leeftijd: 44, sexe:'v', gehuwd:true, 
        kinderen:[{naam:'Patrick', leeftijd: 12, sexe:'m'},{naam:'Jonas', leeftijd: 14, sexe:'m'}], functie:'bediende',id:1025,vrienden:11},
	{leeftijd: 27, sexe:'v', gehuwd:false,id:9007,functie:'arbeider',naam:'Heidi Vercouteren',vrienden:6}
    ];

window.onload = function(){

    //======= DOM REFERENTIES ======================================================
    //knoppen
    var eToevoegen 		= document.getElementById('toevoegen');
    var ePersoneelsLijst= document.getElementById('personeelsLijst');
    //invulvelden, keuzelijsten, etc...
    var eNaam 			= document.getElementById('naam');
    var eFunctie 		= document.getElementById('functie');
    var eSexe 			= document.frmPersoneelslid.sexe;
    var eLeeftijd 		= document.getElementById('leeftijd');
    var eGehuwd 		= document.getElementById('gehuwd');
    var eKind1			= document.getElementById('kind1');
    var eKind2			= document.getElementById('kind2');
    var eKind3			= document.getElementById('kind3');
    //andere
    var eOutput			= document.getElementById('output');
    var eTeller			= document.getElementById('teller');

    //======= KEUZELIJST OPVULLEN ==================================================
    var eDF = document.createDocumentFragment();
    var eOption1 = document.createElement('option');
    var sValue1 = document.createTextNode('--- kies een functie ---');
    eOption1.appendChild(sValue1);
    eOption1.value = '';
    eDF.appendChild(eOption1);
    for (var i=0; i < aFuncties.length; i++) {
        var eOption = document.createElement('option');
        var sValue = document.createTextNode(aFuncties[i]);
        eOption.appendChild(sValue);
        eDF.appendChild(eOption);
    }
    eFunctie.appendChild(eDF);

    //======= EVENT LISTENERS ======================================================
    ePersoneelsLijst.addEventListener('click', function() {
        eOutput.innerHTML = fnToonPersoneel(aoPersoneel);
        fnRegLikeKnoppen();
    }, false);

    eToevoegen.addEventListener('click', function() {
        // formulierwaarden aflezen
        var sNaam =     eNaam.value;
        var nLeeftijd = eLeeftijd.value;
        var sKind1 =    eKind1.value;
        var sKind2 =    eKind2.value;
        var sKind3 =    eKind3.value;
        var bGehuwd =   eGehuwd.checked;
        var sFunctie =  eFunctie.value;

        var sSexe =     undefined;
        //bepaal value sexe
        for (var i=0; i<eSexe.length; i++) {
            if (eSexe[i].checked === true) {
                sSexe = eSexe[i].value;
            }
        }
        console.log(sNaam + nLeeftijd + sFunctie + sSexe + bGehuwd + sKind1 + sKind2 + sKind3);

        //eenvoudige validatie
        if (sNaam ==='' || isNaN(nLeeftijd) || typeof sSexe === 'undefined' || sFunctie === '') {
            //niet goed
            console.log('validatie NIET OK');
            alert('één van de verplichte velden is niet (correct) ingevuld');
        } else {
            //goed
            console.log('validatie OK');
            fnPersoneelslidToevoegen(sNaam, nLeeftijd, bGehuwd, sFunctie, sSexe, [sKind1, sKind2, sKind3]);
            //formulier reset voor volgende toevoeging
            document.frmPersoneelslid.reset();
            eNaam.focus();
        }
    }, false);
};

//======= EVENT HANDLERS =======================================================

function fnToonPersoneel(aoData) {
    //  return HTML string met UL lijst van properties en hun waarden
    //  van alle objecten in @aoData array van objecten
    var sLijst = '';
    if (aoData.length > 0) {
        // overloop array van objecten
        for (var i=0; i<aoData.length; i++) {
            //overloop alle eigenschappen van het object            
            var oPersoon = aoData[i];
            sLijst +=   '<div class="persoon"' +
                        ' id="pers_' + 
                        oPersoon.id + '"' +
                        ' title="personeelsnummer: "' + 
                        oPersoon.id + '"' +
                        ' data-vrienden="' +
                        oPersoon.vrienden + '">';
            for (var key in oPersoon) {
                var propWaarde = oPersoon[key];
                if (key !== 'id' && key !== 'vrienden') {
                    if (Array.isArray(propWaarde)) {
                        sLijst +=   '<span class="prop">' + key + '</span><span class="val">' + fnToonPersoneel(propWaarde) + '</span>';
                    } else {
                        sLijst +=   '<span class="prop">' + key + ':</span><span class="val">' + propWaarde + '</span>';                    
                    }
                }
            }
            if (typeof oPersoon.vrienden !== 'undefined') {
                sLijst += '<button class="like" title="voeg een vriendje toe">Like</button>';
            }
            sLijst += '</div>';
        }
    }
    return sLijst;
}

function fnPersoneelslidToevoegen (naam, leeftijd, gehuwd, functie, sexe, aKindnamen) {
    /* maakt een personeelslid object en voegt toe aan aoPers
     * @naam   String, 
     * @leeftijd Number, 
     * @gehuwd   Boolean, 
     * @functie String, 
     * @sexe   String, 
     * @aKindnamen Array, optioneel, array van kindnamen
     */
    var persoon =           new Object();
    persoon.naam =          naam;
    persoon.leeftijd =      leeftijd;
    persoon.functie =       functie;
    persoon.gehuwd =        gehuwd;
    persoon.sexe =          sexe;
    persoon.vrienden =      0;
    persoon.id =            Math.round((Math.random() * 1000) + 1);
    var aKindNamen =        aKindnamen || []; //optioneel argument opvangen
    var aantalKinderen =    aKindnamen.length;
    if (aantalKinderen > 0) {
        persoon.kinderen =  []; //maak een property
        for (var i=0; i < aantalKinderen; i++) {
            if (aKindNamen[i] !== '') {
                var kind = new Object();
                kind.naam = aKindNamen[i];
                persoon.kinderen.push(kind);
            }
        }
    }
    aoPersoneel.push(persoon);
    //console.log(aoPersoneel)
    fnUpdateTeller(1);
}

function fnUpdateTeller(n) {
    /*  update de teller in de span#teller
     *  @n increment/verhoging
     */
    var eTeller =       document.getElementById('teller');
    var nTeller =       parseInt(eTeller.innerHTML);
    nTeller    +=       n;
    eTeller.innerHTML = nTeller;
}

function fnRegLikeKnoppen() {
    /*
     * event registratie voor de like knoppen
     * kan in JS enkel NA het aanmaken van de knop
     * 
     */
    var eLikes = document.querySelectorAll('.like');
    for (var i=0; i<eLikes.length; i++) {
        eLikes[i].addEventListener('click', function() {
            var ePersoon =          this.parentNode;
            var nVriendjes =        parseInt(ePersoon.dataset['vrienden']) + 1;
            ePersoon.dataset['vrienden'] = nVriendjes;
            alert('Deze persoon heeft er een vriendje bij:' + nVriendjes);
        });
    }
}