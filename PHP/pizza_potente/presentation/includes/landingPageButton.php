<?php
// file     :   presentation/includes/landingPageButton.php
// author   :   sven.croon

// variables:
// -----------------------------------------------------------------------------

?>

<a class='logoutbutton' href='index.php?display=home&logout=true'>
    <img id='logout' src='images/landingPageButton.png' alt='back to landing page'/>
    <p id='logout_text'>Landing Page</p>
</a>