<?php
// file     :   presentation/includes/nologin.php
// author   :   sven.croon

// variables:
// -----------------------------------------------------------------------------
?>

    <label for='email' class='obscure'>email</label>
    <input class='obscure' type='email' name='email' disabled/>
    <label for='password' class='obscure'>password</label>
    <input class='obscure' type='password' name='password' disabled/>
    <input type='submit' name='submit' id='submit' value='submit'/>
</form>
