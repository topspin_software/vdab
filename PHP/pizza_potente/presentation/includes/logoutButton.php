<?php
// file     :   presentation/includes/logoutButton.php
// author   :   sven.croon

// variables:
// -----------------------------------------------------------------------------

?>

<a class='logoutbutton' href='index.php?display=home&logout=true'>
    <img id='logout' src='images/logout-button.svg' alt='logout'/>
    <p id='logout_text'>Logout</p>
</a>

