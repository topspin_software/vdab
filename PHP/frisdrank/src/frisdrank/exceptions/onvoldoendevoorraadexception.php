<?php
namespace frisdrank\exceptions;

use \Exception;

class OnvoldoendeVoorraadException extends Exception{
	
	protected $ErrorMessage1 = "Er is onvoldoende voorraad van het product ";

	public  function getErrorMessage (){
		return $this->ErrorMessage1 . $this->getMessage();
		}
	
	}

?>