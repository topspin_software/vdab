<?php
namespace frisdrank\exceptions;

use \Exception;

class OnvoldoendeMuntenException extends Exception{
	
	protected $ErrorMessage1 = "Er zijn onvoldoende munten van ";
	protected $ErrorMessage2 = " om te kunnen teruggeven, aankoop geannulleerd";

	public  function getErrorMessage (){
		return $this->ErrorMessage1 . $this->getMessage() .$this->ErrorMessage2 ;
		}
	
	}

?>