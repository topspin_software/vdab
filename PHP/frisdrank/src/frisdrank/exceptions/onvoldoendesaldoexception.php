<?php
namespace frisdrank\exceptions;

use \Exception;

class OnvoldoendeSaldoException extends Exception{
	
	protected $ErrorMessage = "Dit saldo is onvoldoende voor dit drankje";
	
	public  function getErrorMessage (){
		return $this->ErrorMessage;
		
		}
	
	}

?>