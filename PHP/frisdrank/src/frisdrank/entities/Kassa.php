<?php

namespace frisdrank\entities; 
//de namespace is "frisdrank"
//namespaces namen zelfde case als directory namen

use stdClass; //namespace gebruiken

//ORM annotation

/**
* 	@Entity
	@Table(name="tblkassa")
*/
class Kassa{

	//singleton
	private static $inst = null;

	/**
	*	@Id
	*	@Column(type="integer")
	*/
	private $id;
	/**
	*	@Column(type="integer")
	*/
	private $euro2;
	/**
	*	@Column(type="integer")
	*/
	private $euro1;
	/**
	*	@Column(type="integer")
	*/
	private $cent50;
	/**
	*	@Column(type="integer")
	*/
	private $cent20;
	/**
	*	@Column(type="integer")
	*/
	private $cent10;

	//singleton
	public static function getInstance()
		{
			if ($this->inst === null) {
				$this->inst = new Kassa();
			}
			return $this->inst;
		}
	/**
     * Private ctor zodat hij niet gebruikt kan worden, enkel private
     *
     */
    private function __construct()
    {

    }
	
	//db met één record: id=0
	
	public function setEuro2($euro2){
		$this->euro2=$euro2;
		}
	public function setEuro1($euro1){
		$this->euro2=$euro2;
		}
	public function setCent50($cent50){
		$this->cent50=$cent50;
		}
	public function setCent20($cent20){
		$this->cent20=$cent20;
		}
	public function setCent10($cent10){
		$this->cent10=$cent10;
		}

	
	
	
	public function getEuro2(){
		return $this->euro2;
		}
	public function getEuro1(){
		return $this->euro1;
		}
	public function getCent50(){
		return $this->cent50;
		}
	public function getCent20(){
		return $this->cent20;
		}
	public function getCent10(){
		return $this->cent10;
		}

	public function toStdClass(){
		//naar stdClass
		$obj		= new stdClass;
		
		$obj->euro2		= $this->getEuro2();
		$obj->euro1		= $this->getEuro1();
		$obj->cent50	= $this->getCent50();
		$obj->cent20	= $this->getCent20();
		$obj->cent10	= $this->getCent10();
		
		return $obj;
		}
	public function toArray()
	//maakt er een array van, via de stdClass
	  {
		  $array = array();
		  $std= $this->toStdClass();
		  foreach ($std as $key => $value) {
				  $array[$key] = $value;
		  }
		  return $array;
	  }
	}

?>