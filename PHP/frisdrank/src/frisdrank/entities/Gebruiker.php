<?php

namespace frisdrank\entities; 
//de namespace is "frisdrank"
//namespaces namen zelfde case als directory namen

use stdClass; //namespace gebruiken

//ORM annotation: deze entity komt overeen met de table tblgebruikers


/**
* 	@Entity
	@Table(name="tblgebruikers")
*/
class Gebruiker{
	//indien geen name="$id" gaat Doc ervan uit dat die dezelfdz  zijn als de table columnbs
	//vlak boven de private vars
	//@Id is de aanduiding van de PK
	
	/**
	*	@Id
	*	@Column(type="integer")
	*	@Generated(strategy="AUTO")
	*/
	private $id;
	/**
	*	@Column(type="text", length=100)
	*/
	private $email;
	
	/**
	*	@Column(type="text", length=100)
	*/
	private $password;
	
	//geen setId want autoincrement die ga je nooit zetten
	public function setEmail($email){
		$this->email=$email;
		}
	public function setPassword($password){
		$this->password=$password;
		}
	public function getId(){
		return $this->id;
		}
	public function getEmail(){
		return $this->email;
		}
	public function getPassword(){
		return $this->password;
		}	
		
	public function toStdClass(){
		//naar stdClass
		$obj		= new stdClass;
		$obj->id 	= $this->getId();
		$obj->email = $this->getEmail();
		$obj->password = $this->getPassword();
		return $obj;
		}
	
	}

?>