<?php

namespace frisdrank\entities; 
//de namespace is "frisdrank"
//namespaces namen zelfde case als directory namen
use stdClass; //namespace gebruiken

//ORM annotation: deze entity komt overeen met de table tblfrisdrank

/**
* 	@Entity
	@Table(name="tblfrisdrank")
*/
class Frisdrank{
	
	/**
	*	@Id
	*	@Column(type="integer")
	*	@Generated(strategy="AUTO")
	*/
	private $id;
	
	/**
	*	@Column(type="string", length=40)
	*/
	private $naam; 		
	
	/**
	*	@Column(type="float")
	*/
	private $prijs; 
	
	/**
	*	@Column(type="integer")
	*/
	private $aantal; 
	
	/**
	*	@Column(type="string", length=100)
	*/
	private $foto; 	
	
	
	
	
	public function setNaam($naam){
		$this->naam=$naam;
		}
	public function setPrijs($prijs){
		$this->prijs=$prijs;
		}	
	public function setAantal($aantal){
		$this->aantal=$aantal;
		}	
	public function setFoto($foto){
		$this->foto=$foto;
		}	


	public function getId(){
		return $this->id;
		}
	public function getNaam(){
		return $this->naam;
		}
	public function getPrijs(){
		return $this->prijs;
		}		
	public function getAantal(){
		return $this->aantal;
		}	
	public function getFoto(){
		return $this->foto;
		}	

	
	public function toStdClass(){
		//naar stdClass
		$obj			= new stdClass;
		$obj->id 		= $this->getId();
		$obj->prijs 	= $this->getPrijs();
		$obj->aantal 	= $this->getAantal();
		$obj->foto 		= $this->getFoto();

		return $obj;
		}
	}

?>