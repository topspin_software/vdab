<?php

namespace frisdrank\business;

use stdClass; //in de root namespace

class HTMLService {
	
	public function getFrisdranktabel($aoLijst){

			$output = "<ul class='frisdranklijst clearFix'>";
			//lus doorheen entities
			
			foreach($aoLijst as $obj){
				
				if($obj->getAantal()<=0){
					$strKopen="<span class='uitverkocht'>uitverkocht</span>";
					}
				else{
					$strKopen="<p><button class='dranken' data-prijs='".$obj->getPrijs()."'  data-name='".$obj->getNaam()."' value='".$obj->getId(). "'>koop</button>";
				}
				
				$output .= "<li>";
				
				$output .= "<h3>";
				$output .= $obj->getNaam();
				$output .= "</h3>";
				
				$output .= "<p>prijs: ";
				$output .= $obj->getPrijs();
				$output .= "</p>";
				
				$output .= "<img src='img/";
				$output .= $obj->getFoto();
				$output .= "' />";
				
				$output .= "</p>";
				$output .= $strKopen;
				$output .= "</p>";
				
				$output .= "</li>";
				}
			$output .= "</ul>";
		
			return $output;
		
			}
			
		public function getMuntenTerugLijst($aTerug){

			$output = "<ul class='wisselgeldlijst clearFix'>";
			//lus doorheen entities
			$muntenCollectie = array("2 euro","1 Euro","50 cent","20 cent","10 cent");
			$i=0;
			foreach($aTerug as $key=>$value){
				$output .= "<li>".$muntenCollectie[$i]." : ".$value. "</li>";
				$i++;
				}
			$output .= "</ul>";
		
			return $output;
		
			}		

	}