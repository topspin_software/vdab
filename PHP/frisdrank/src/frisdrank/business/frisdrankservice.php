<?php
namespace frisdrank\business;

//use stdClass; //in de root namespace
use frisdrank\data\FrisdrankDAO;
use frisdrank\data\GebruikerDAO;
use frisdrank\data\KassaDAO;
use frisdrank\exceptions\OnvoldoendeMuntenException;
use frisdrank\exceptions\OnvoldoendeSaldoException;
//require("src/frisdrank/exceptions/onvoldoendemuntenexception.php");

class FrisdrankService {
	
	private $muntenCollectie = array("euro2"=>200,"euro1"=>100,"cent50"=>50,"cent20"=>20,"cent10"=>10);
	
	public function getFrisdrankOverzicht(){

		$frisdranken = FrisdrankDAO::getAll();
		return $frisdranken; // hier geen formaat kiezen: de controller bapaalt het formaat
		}
	
	
	public function koopFrisdrank($id,$saldo){	
		//koopfunctie
		//converteren * 100, gehele getallen, anders problemen met floats die nooit echt 0 worden...
		$frisdrank 	= FrisdrankDAO::getById($id);
		$saldo 		= intval($saldo * 100);
		//bestaat deze drank
		if($frisdrank){
			
			$prijs 		= $frisdrank->getPrijs();
			$prijs 		= intval($prijs * 100);
			$voorraad 	= $frisdrank->getAantal();
			
			//print_r($prijs);
			if($prijs>$saldo){
				//onvoldoende geld
				throw new OnvoldoendeSaldoException(); 
				}
			else{
				//voldoende geld
				$bedragTerug =  $saldo-$prijs;
				//print_r($bedragTerug);
				if($voorraad ==0){
					throw new OnvoldoendeVoorraadException($frisdrank->getNaam()); 
					}
				else{
					//verminder voorraad met 1 item van dit product
					$aantal 	= FrisdrankDAO::verkoopById($id); //returnwaarde is aantal geupdate rijen
					//return $aantal;
					//wisselgeld
					$aMunten = $this->getWisselMunten($bedragTerug);
					return $aMunten;
				}
				
				}
			}
	}
	
	
	public function getWisselMunten($bedrag){
		
		$muntenTerug 	= array(200=>0, 100=>0, 50=>0, 20=>0, 10=>0);
		//print_r($muntenTerug);
		$kassa 			= KassaDAO::getKassa();
		$aKassa 		= $kassa->toArray();
		
		foreach($this->muntenCollectie as $key => $value){
			
//			print_r("bedrag voor:". $bedrag."<br>");
//			print_r("key:". $key . "value:". $value .",in kassa:". $aKassa[$key]."<br>");
			
			$aantal 			= floor($bedrag / $value); //geheel aantal munten
			if($aantal>$aKassa[$key]){throw new OnvoldoendeMuntenException($key); } //niet genoeg in voorraad
			
			$muntenTerug[$value]= $muntenTerug[$value]+$aantal; 
			$bedrag				= fmod($bedrag,$value); //restbedrag
			if($bedrag==0){		break;	}
			
//			//tests
//			print_r("bedrag na:". $bedrag."<br>");
//			print_r("aantal:". $aantal."bedrag na:". $bedrag."<br>");
//			print_r("==========<br>");
//			
			}
		return $muntenTerug;
		}
		
}