<?php

namespace frisdrank\data; 

class FrisdrankDAO extends AbstractDAO{
	
	
	
	public static function getAll(){
		
		$em = parent::getEntityManager();
		//dql enkel met classes en properties
		$query=$em->createQuery('SELECT f FROM frisdrank\entities\Frisdrank f ORDER BY f.naam');
		return $query->getResult(); //geeft een array van objecten van die klasse
		}
			
	public static function getById($id){
		
		$em = parent::getEntityManager();
		//dql enkel met classes en properties
		$query=$em->createQuery('SELECT f FROM frisdrank\entities\Frisdrank f WHERE f.id = :pId'); //f.id = property; pId parameter
		$query->setParameter("pId",$id); 
		//kan enkele parameter uitvoeren op een geldig queryobject daarom erna
		
		return $query->getOneOrNullResult(); //geeft een array van objecten van die klasse
		}	
	
	public static function verkoopById($id){
		///vermindert voorraad met 1
		$em = parent::getEntityManager();
		//even proberen met de querybuilder
		$qb = $em->createQueryBuilder();
		$q 	= $qb->update('frisdrank\entities\Frisdrank', 'f')
				->set('f.aantal', 'f.aantal-1')
				->where('f.id = ?1')
				->setParameter(1, $id)
				->getQuery();
		$p = $q->execute();
		//$p = aantal rijen affected
		return $p;
		
		//klassieke manier
	}

	
	
	}