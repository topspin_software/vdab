<?php

namespace frisdrank\data; 
require_once("vendors/Doctrine/ORM/Tools/Setup.php");

use Doctrine\ORM\Tools\Setup; //optioneel , enkele voor kortere code 
use Doctrine\ORM\EntityManager; //kortere code later

abstract class AbstractDAO{
	
	//singleton
	private static $entityManager = null;

	//Doctrine levert een EntityManager die alle DAO beheert
	protected  static function getEntityManager(){
		
		if(is_null(self::$entityManager)){
			Setup::registerAutoLoadDirectory("vendors");
			$entityPath =array("src/frisdrank/entities");
			$dbParams = array(
				"driver" 	=> "pdo_mysql",
				"user"		=> "web",
				"password" 	=> "web",
				"dbname" 	=> "frisdrank"
			);
			
			$config = Setup::createAnnotationMetaDataConfiguration($entityPath);
			
			//proxy objecten voor joins met andere objecten: 
			//zet een script in die dir
			//daarna mag je de auatgenearet commentarieren
			$config->setProxyDir("src/frisdrank/proxies");
			$config->setProxyNamespace('frisdrank\proxies');//namespace forward slashes
			$config->setAutoGenerateProxyClasses(true); //om proxy objecten aan te maken, 
			// eenmaal aangemaakt deze lij n commentarieren
			// OPNIEUW TE GENREREN BIJ ELKE ENTITY WIJZIGING
			
			$em 	= EntityManager::create($dbParams,$config);
			self::$entityManager = $em;
			return $em;
		}
		else {
			return self::$entityManager;
		}
		}	
	
	}
	