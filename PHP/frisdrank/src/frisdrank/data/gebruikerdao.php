<?php

namespace frisdrank\data; 

class GebruikerDAO extends AbstractDAO{
	
	
	
	public static function getAll(){
		
		$em = parent::getEntityManager();
		//dql enkel met classes en properties
		$query=$em->createQuery('SELECT f FROM frisdrank\entities\Gebruiker f ORDER BY f.email');
		return $query->getResult(); //geeft een array van objecten van die klasse
		}
			
	public static function getById($id){
		
		$em = parent::getEntityManager();
		//dql enkel met classes en properties
		$query=$em->createQuery('SELECT f FROM frisdrank\entities\Gebruiker f WHERE f.id = :pId'); //f.id = property; pId parameter
		$query->setParameter("pId",$id); 
		
		return $query->getOneOrNullResult(); //geeft een array van objecten van die klasse
		}	
	
	public static function getByEmail($email){
		
		$em = parent::getEntityManager();
		//dql enkel met classes en properties
		$query=$em->createQuery('SELECT f FROM frisdrank\entities\Gebruiker f WHERE f.email = :pEmail'); 
		// pEmail parameter
		$query->setParameter("pEmail",$email); 
		
		return $query->getOneOrNullResult(); //geeft een array van objecten van die klasse
		}	
	
	
	}