<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Frisdrankmachine met doctrine PHP</title>
<style>
body {
	font-family:Verdana, Geneva, sans-serif;
}
div {
	margin:0;
	padding:0;
	box-sizing:border-box;
}
.wrapper {
	width: 90%;
	margin: 0 auto;
}
.inhoud {
	box-sizing:border-box;
}
.left {
	float:left;
	width: 58%;
	margin-right:10px;
}
.right {
	float:left;
	width: 40%;
	;
}
.paneel {
	background:#eee;
	padding:1em;
}
.nobullet {
	list-style-type:none;
}
.frisdranklijst {
	list-style-type:none;
	font-size:0.9em;									/* 12px = 1em */
	width:100%;
	margin:0;
	padding:0;									/* 0.5em = 6px */
}
.frisdranklijst li {
	box-sizing:border-box;
	float:left;
	width: 32%;										/* 2 naast elkaar*/
	padding:0.2em;
	margin:0.2em;
	background:lavender;
}
.frisdranklijst h3 {
	color:darkgreen;
	margin:0;
	padding:0.2em 0 0 0;
	width:100%;
	font-size:1.125em; 								/* 18px */
}
.frisdranklijst img {
	float:none;
	margin: 0;
	max-width:90%;
	border:5px solid snow;
	border-radius:0.2em;
}
.uitverkocht {
	color:red;
	font-weight:bold;
}
/*clearfix hack*/
.clearFix:before, .clearFix:after {
	content: " ";
	display: table;
}
.clearFix:after {
	clear: both;
}
/* enkel voor IE6/7  */ 
.clearFix {
 *zoom: 1;
}
</style>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" >
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script> (window.jQuery || document.write('<script src="src/presentation/js/vendors/jquery/js/jquery-1.10.1.min.js"><\/script>'))</script>
<script>

$(function(){
	
//	var oTable = $("#filmlijst").dataTable({
//		"sAjaxSource": "ajax_getOverzicht.php"
//		})
		var $velden 		= $('#betalen input');//collection
		var $betaald 		= $('#betaald');
		var $gekochtNaam 	= $('#gekochtNaam');
		var $gekochtWaarde 	= $('#gekochtWaarde');
		var $fdId 			= $('#fdId');
		
		//munten hyperlinks
		$('.munten').on('click',function(e){
			
			e.preventDefault();
			//de input ernaast
			var eInput 		= this.parentNode.querySelector('input');
			//waarde die nu in de input staat
			var nInputVal	= parseFloat($(eInput).val());
			//console.log(nInputVal);
			var nInputVal 	= ++nInputVal;
			$(eInput).val(nInputVal);
			
			//plaats totaal in form field
			$betaald.val(totaliseerMunten());
			
			})
		
		//frisdrank koopknoppen==============
		$('.dranken').on('click',function(e){
			e.preventDefault();
			//eerst geld inwerpen
			var id 		= $(this).val();
			var naam 	= $(this).data('name');
			var prijs 	= $(this).data('prijs');
			console.log(id,naam,prijs);
			
			
				$gekochtNaam.val(naam);		//tekst
				$gekochtWaarde.val(prijs);	//getal
				$fdId.val(id);				//id in hidden field
				
			})
			
		//submit van form ==============
		$('#frmKoop').on('submit',function(e){
			e.preventDefault();
			//controle of er iets gekocht is en of er voldoende geld is
			var $saldo 		= $betaald.val(); 		// betaald 
			var $teBetalen 	= $gekochtWaarde.val(); // te betalen
			
			if($saldo=="" || $saldo==0){
				alert("U moet eerst voldoende geld inwerpen");
				}
			else {
				if($teBetalen=="" || $teBetalen==0){
					alert("Kies eerst een drankje");
					}
					else{
						//alles OK
						$('#frmKoop')[0].submit();
					}
				}
			})
		//=============================	
		function totaliseerMunten(){
			//console.log($velden.length)
			nTotaal = 0;
			nAantal= $velden.length;
			
			for(var i=0;i<nAantal;i++){
				var $veld = $($velden[i]);
				var nVeldWaarde = parseFloat($veld.data('value'));
				var nVeldAantal = parseInt($veld.val());
				var nSubTotaal = nVeldWaarde * nVeldAantal;
				//console.log(nVeldAantal," " , nVeldWaarde, " ",nSubTotaal  );
				nTotaal = parseFloat(nTotaal) + nSubTotaal;
				}
			//console.log("tot", nTotaal);
			return nTotaal.toFixed(2); 
			//toFixed nodig want anders een flaoting point met 10 decimalen! -> hieruit komen de problemen in PHP
			}


	})
</script>
</head>

<body>
<div class="wrapper">
  <header> </header>
  <section class="inhoud clearFix">
    <h1>Frisdrankmachine Doctrine PHP jQuery</h1>
    <?php
	  if(!empty($error)){
		  print("<div class='error'>$error</div>");
		  }
	 ?>
    <div class="left paneel">
      <?php
	//print_r($lijst);
	print($frisdrankTabel);
	?>
    </div>
    <!--einde left-->
    
    <div class="right">
      <div class="paneel">
        <h4>Werp muntstukken in, kies een frisdrank, klik dan op 'Koop'</h4>
        <ul class="nobullet" id="betalen">
          <li><a href="#"  alt="2 euro" class="munten"><img src="img/2euro.jpg" alt="2 euro" title="2 euro" /></a> <b>X</b>
            <input type="text" data-value="2" id="euro2" name="euro2" value="0" />
          </li>
          <li><a href="#" alt="1 euro" class="munten"><img src="img/1euro.jpg" alt="1 euro" title="1 euro"/></a> <b>X</b>
            <input type="text" data-value="1" id="euro1" name="euro1" value="0" />
          </li>
          <li><a href="#" alt=" 50 cent" class="munten"><img src="img/50cent.jpg" alt=" 50 cent" title=" 50 cent"/></a> <b>X</b>
            <input type="text" data-value="0.50" id="50cent" name="cent50" value="0" />
          </li>
          <li><a href="#" alt="20 cent" class="munten"><img src="img/20cent.jpg"alt="20 cent" title="20 cent" /></a> <b>X</b>
            <input type="text" data-value="0.20" id="cent20" name="cent20" value="0" />
          </li>
          <li><a href="#" alt="10 cent" class="munten"><img src="img/10cent.jpg" alt="10 cent" title="10 cent"/></a> <b>X</b>
            <input type="text" data-value="0.10" id="cent10" name="cent10" value="0" />
          </li>
        </ul>
        <form method="get" action="index.php" id="frmKoop" name="frmKoop">
          <input type="hidden" name="action" value="koop" />
          <input type="hidden" id="fdId" name="fdId" value="" />
          <div>
            <label>ingeworpen : </label>
            <input type="text" id="betaald" name="betaald" value="0" />
          </div>
          <div>
            <label>gekocht : </label>
            <input id='gekochtNaam' name="gekochtNaam" type="text" value="" />
            <input id='gekochtWaarde' name="gekochtWaarde" type="text" value="0" />
          </div>
          <div>
            <label></label>
            <input type="submit" id="klaar" value="Koop" />
          </div>
        </form>
        <h4>Teruggave:</h4>
        <p>Uw wisselgeld:
          <?php 
//		  	if(isset($aTerug)) {
//				print_r($aTerug); 
//			}

			if(isset($strTerug)) print($strTerug); 
			
			?>
        </p>
        <p><a href="index.php">Opnieuw</a></p>
      </div>
    </div>
    <!--einde right--> 
    
  </section>
</div>
</body>
</html>