<?php
error_reporting(E_ALL);
require_once("src/frisdrank/exceptions/onvoldoendesaldoexception.php");
require_once("src/frisdrank/exceptions/onvoldoendemuntenexception.php");
require_once("src/frisdrank/exceptions/onvoldoendevoorraadexception.php");

//namespaces gebruiken met de doctrine Classloader: geen eigen requires nodig
//enkel de nodig entities zullen geladen worden door Doctrine
require_once("vendors/Doctrine/Common/ClassLoader.php");
//Doctrine ClasLoader heeft eigen namespace
$classLoader = new Doctrine\Common\ClassLoader("frisdrank","src");
$classLoader->register();


//========controller=========
use frisdrank\business\FrisdrankService;
use frisdrank\business\HTMLService;

$fds = new FrisdrankService();
$htmls =new HTMLService();

//de frisdrankenlijst
$lijst = $fds->getFrisdrankOverzicht(); //array met objecten
$frisdrankTabel= $htmls->getFrisdranktabel($lijst); //HTML

//iets kopen
if (isset($_GET['action']) && $_GET['action']=="koop"){
	
	$fdId 		= $_GET['fdId'];
	$betaald 	= $_GET['betaald'];
	
			
	try {
		try{
			try {
				$aTerug = $fds->koopFrisdrank($fdId,$betaald); //return array	
				//$strTerug = $aTerug;//test van return value UPDATE dql
				//verwerkign return array
				$strTerug = $htmls->getMuntenTerugLijst($aTerug);
			}
			catch(OnvoldoendeVoorraadException $ove){
				$msg=$tbe->getErrorMessage(); 
				header("location: index.php?id=".$_POST["id"]."&error=".$msg);
				exit(0);	
				}	
			}
		catch(OnvoldoendeMuntenException $ome){
			$msg=$tbe->getErrorMessage(); 
			header("location: index.php?id=".$_POST["id"]."&error=".$msg);
			exit(0);
		}	
	}
	catch(OnvoldoendeSaldoException $ose){
		$msg=$tbe->getErrorMessage(); 
		header("location: index.php?id=".$_POST["id"]."&error=".$msg);
		exit(0);
		
	}
		
	
	
	}
	
if(isset($_GET["error"])) $error = $_GET["error"];

//==========HTML functions======


//==========presentatie======
include('src/frisdrank/presentation/pres.frisdrankmachine.php');
