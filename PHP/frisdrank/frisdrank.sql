-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Machine: 127.0.0.1
-- Genereertijd: 17 jun 2013 om 10:40
-- Serverversie: 5.5.27
-- PHP-versie: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databank: `frisdrank`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tblfrisdrank`
--

CREATE TABLE IF NOT EXISTS `tblfrisdrank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naam` varchar(40) NOT NULL,
  `prijs` float NOT NULL,
  `aantal` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Gegevens worden uitgevoerd voor tabel `tblfrisdrank`
--

INSERT INTO `tblfrisdrank` (`id`, `naam`, `prijs`, `aantal`, `foto`) VALUES
(1, 'Coca-Cola', 1.2, 5, 'cola.jpg'),
(2, 'Fanta', 1.2, 20, 'fanta.jpg'),
(3, 'Cola-Light', 1.2, 20, 'colalight.jpg'),
(4, 'Spa bruis', 1, 0, 'spabruis.jpg'),
(5, 'Ice Tea', 1.6, 18, 'icetea.jpg'),
(6, 'Fruitsap Sinaas', 1.3, 20, 'orange.jpg'),
(7, '7up', 1, 20, '7up.jpg');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tblgebruikers`
--

CREATE TABLE IF NOT EXISTS `tblgebruikers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Gegevens worden uitgevoerd voor tabel `tblgebruikers`
--

INSERT INTO `tblgebruikers` (`id`, `email`, `password`) VALUES
(1, 'jan.vandorpe@vdab.be', 'vdab'),
(2, 'carole_ampe@yahoo.co.uk', 'hluti802');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `tblkassa`
--

CREATE TABLE IF NOT EXISTS `tblkassa` (
  `id` int(11) NOT NULL,
  `euro2` int(11) NOT NULL,
  `euro1` int(11) NOT NULL,
  `cent50` int(11) NOT NULL,
  `cent20` int(11) NOT NULL,
  `cent10` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden uitgevoerd voor tabel `tblkassa`
--

INSERT INTO `tblkassa` (`id`, `euro2`, `euro1`, `cent50`, `cent20`, `cent10`) VALUES
(0, 10, 10, 10, 10, 10);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
