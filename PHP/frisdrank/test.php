<?php
//testbestand voor services en DAOs
error_reporting(E_ALL);
//require_once("src/frisdrank/exceptions/onvoldoendesaldoexception.php");
//require_once("src/frisdrank/exceptions/onvoldoendemuntenexception.php");

//namespaces gebruiken met de doctrine Classloader: geen eigen requires nodig
//enkel de nodig entities zullen geladen worden door Doctrine
require_once("vendors/Doctrine/Common/ClassLoader.php");
//Doctrine ClasLoader heeft eigen namespace
$classLoader = new Doctrine\Common\ClassLoader("frisdrank","src");
$classLoader->register();

////DAO test
//use frisdrank\data\KassaDAO;
//$kassa = KassaDAO::getKassa();
//print_r($kassa);

//frisdrankservice test
use frisdrank\business\FrisdrankService;
//require_once("src/frisdrank/business/frisdrankservice.php");

$fds = new FrisdrankService();

//direct naat wisselfucntie
//$bedrag = 0.40; //probeer 700 voor exception
//$terug= $fds->getWisselMunten($bedrag);
//print("munten breakdown voor" . $bedrag. " = ");
//print_r($terug);

//koopservice
$id = 5; //probeer 700 voor exception
$saldo= 2;
$terug= $fds->koopFrisdrank($id,$saldo);
print("teruggave voor id " . $id . " met betaald= " .$saldo . "<br>");
print_r($terug);


?>