-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2016 at 03:47 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cursusphp`
--

-- --------------------------------------------------------

--
-- Table structure for table `chatroom`
--

CREATE TABLE `chatroom` (
  `user` tinytext NOT NULL,
  `message` text NOT NULL,
  `timestamp` date NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chatroom`
--

INSERT INTO `chatroom` (`user`, `message`, `timestamp`, `id`) VALUES
('Joey', 'Yo wassup ppl\r\nkto pl', '2016-02-01', 1),
('Admin', 'Please be nice', '2016-02-01', 4),
('Ikke', 'jkust ze', '2016-02-01', 5),
('siggy', 'jawadde', '2016-02-01', 6),
('siggy', 'jawadde', '2016-02-01', 7),
('Svekke', 'jawadde ik zen het hier muug :-)\r\n', '2016-02-01', 8),
('Svekke', 'jawadde ik zen het hier muug :-)\r\n', '2016-02-01', 9);

-- --------------------------------------------------------

--
-- Table structure for table `films`
--

CREATE TABLE `films` (
  `id` int(10) UNSIGNED NOT NULL,
  `titel` varchar(150) NOT NULL,
  `duurtijd` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `films`
--

INSERT INTO `films` (`id`, `titel`, `duurtijd`) VALUES
(1, 'Shawshank Redemption, The', 142),
(2, 'Godfather, The', 168),
(3, 'Green Mile, The', 188),
(4, 'Pulp Fiction', 154),
(5, 'Once Upon a Time in the West', 165),
(6, 'Lord of the Rings: The Return of the King, The', 201),
(7, 'Se7en', 122),
(8, 'Schindler''s List', 195),
(9, 'Forrest Gump', 142),
(11, 'Loft', 98),
(12, 'Flodder', 93);

-- --------------------------------------------------------

--
-- Table structure for table `four-in-a-row`
--

CREATE TABLE `four-in-a-row` (
  `ID` int(11) NOT NULL,
  `user1` text NOT NULL,
  `user2` text NOT NULL,
  `user1_color` text NOT NULL,
  `user2_color` text NOT NULL,
  `start_color` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `four-in-a-row-game`
--

CREATE TABLE `four-in-a-row-game` (
  `ID` int(11) NOT NULL,
  `game_ID` int(11) NOT NULL,
  `round` int(11) NOT NULL,
  `column_nr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gastenboek`
--

CREATE TABLE `gastenboek` (
  `id` int(10) UNSIGNED NOT NULL,
  `auteur` varchar(45) NOT NULL,
  `boodschap` varchar(250) NOT NULL,
  `datum` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gastenboek`
--

INSERT INTO `gastenboek` (`id`, `auteur`, `boodschap`, `datum`) VALUES
(2, 'Bezoeker', 'Even testen of het gastenboek werkt...', '2010-05-11 09:22:44'),
(3, 'Admin', 'Het werkt inderdaad.', '2010-05-11 09:24:13'),
(12, 'Svekke', 'Test 1 2 3', '2016-02-01 11:24:12'),
(14, 'Pieje Stoef', 'Ik zen den beste', '2016-02-01 11:24:49'),
(16, 'Jeanke De metser', 'Veu moa een groete pint!', '2016-02-01 11:28:42');

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `naam` varchar(50) NOT NULL,
  `prijs` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `naam`, `prijs`) VALUES
(1, 'Programmatielogica', 75),
(2, 'Computers en netwerken', 130),
(4, 'SQL', 99.9),
(5, 'Objectgeoriënteerde principes', 85),
(6, 'Javascript / DOM', 140),
(7, 'JQuery', 120),
(8, 'UML', 90),
(9, 'PHP', 140),
(11, 'XHTML/CSS', 120);

-- --------------------------------------------------------

--
-- Table structure for table `personen`
--

CREATE TABLE `personen` (
  `id` int(10) UNSIGNED NOT NULL,
  `familienaam` varchar(50) NOT NULL,
  `voornaam` varchar(30) NOT NULL,
  `geboortedatum` date NOT NULL,
  `geslacht` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `personen`
--

INSERT INTO `personen` (`id`, `familienaam`, `voornaam`, `geboortedatum`, `geslacht`) VALUES
(1, 'Peeters', 'Bram', '1976-01-19', 'M'),
(2, 'Van Dessel', 'Rudy', '1969-10-05', 'M'),
(3, 'Vereecken', 'Marie', '1981-05-23', 'V'),
(4, 'Maes', 'Eveline', '1983-08-16', 'V'),
(5, 'Vangeel', 'Joke', '1976-05-22', 'V'),
(6, 'Van Heule', 'Pieter', '1968-03-02', 'M'),
(7, 'Naessens', 'Katleen', '1984-05-12', 'V'),
(8, 'Sleeuwaert', 'Koen', '1957-02-25', 'M');

-- --------------------------------------------------------

--
-- Table structure for table `vieropeenrij_spelbord`
--

CREATE TABLE `vieropeenrij_spelbord` (
  `rijnummer` smallint(5) UNSIGNED NOT NULL,
  `kolomnummer` smallint(5) UNSIGNED NOT NULL,
  `status` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vieropeenrij_spelbord`
--

INSERT INTO `vieropeenrij_spelbord` (`rijnummer`, `kolomnummer`, `status`) VALUES
(1, 1, 0),
(1, 2, 0),
(1, 3, 0),
(1, 4, 0),
(1, 5, 0),
(1, 6, 0),
(1, 7, 0),
(2, 1, 0),
(2, 2, 0),
(2, 3, 0),
(2, 4, 0),
(2, 5, 0),
(2, 6, 0),
(2, 7, 0),
(3, 1, 0),
(3, 2, 0),
(3, 3, 0),
(3, 4, 0),
(3, 5, 0),
(3, 6, 0),
(3, 7, 0),
(4, 1, 0),
(4, 2, 0),
(4, 3, 0),
(4, 4, 0),
(4, 5, 0),
(4, 6, 0),
(4, 7, 0),
(5, 1, 0),
(5, 2, 0),
(5, 3, 0),
(5, 4, 0),
(5, 5, 0),
(5, 6, 0),
(5, 7, 0),
(6, 1, 0),
(6, 2, 0),
(6, 3, 0),
(6, 4, 0),
(6, 5, 0),
(6, 6, 0),
(6, 7, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chatroom`
--
ALTER TABLE `chatroom`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `four-in-a-row`
--
ALTER TABLE `four-in-a-row`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `four-in-a-row-game`
--
ALTER TABLE `four-in-a-row-game`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `game_ID` (`game_ID`);

--
-- Indexes for table `gastenboek`
--
ALTER TABLE `gastenboek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `personen`
--
ALTER TABLE `personen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vieropeenrij_spelbord`
--
ALTER TABLE `vieropeenrij_spelbord`
  ADD PRIMARY KEY (`rijnummer`,`kolomnummer`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chatroom`
--
ALTER TABLE `chatroom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `films`
--
ALTER TABLE `films`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `four-in-a-row`
--
ALTER TABLE `four-in-a-row`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `four-in-a-row-game`
--
ALTER TABLE `four-in-a-row-game`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gastenboek`
--
ALTER TABLE `gastenboek`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `personen`
--
ALTER TABLE `personen`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `four-in-a-row-game`
--
ALTER TABLE `four-in-a-row-game`
  ADD CONSTRAINT `four-in-a-row-game_ibfk_1` FOREIGN KEY (`game_ID`) REFERENCES `four-in-a-row` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
