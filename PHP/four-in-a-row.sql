-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2016 at 03:57 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `four-in-a-row`
--

-- --------------------------------------------------------

--
-- Table structure for table `four-in-a-row-game`
--

CREATE TABLE `four-in-a-row-game` (
  `game_ID` int(11) NOT NULL,
  `player1` text NOT NULL,
  `color1` tinyint(4) NOT NULL,
  `player2` text NOT NULL,
  `round0` int(11) NOT NULL,
  `round1` int(11) NOT NULL,
  `round2` int(11) NOT NULL,
  `round3` int(11) NOT NULL,
  `round4` int(11) NOT NULL,
  `round5` int(11) NOT NULL,
  `round6` int(11) NOT NULL,
  `round7` int(11) NOT NULL,
  `round8` int(11) NOT NULL,
  `round9` int(11) NOT NULL,
  `round10` int(11) NOT NULL,
  `round11` int(11) NOT NULL,
  `round12` int(11) NOT NULL,
  `round13` int(11) NOT NULL,
  `round14` int(11) NOT NULL,
  `round15` int(11) NOT NULL,
  `round16` int(11) NOT NULL,
  `round17` int(11) NOT NULL,
  `round18` int(11) NOT NULL,
  `round19` int(11) NOT NULL,
  `round20` int(11) NOT NULL,
  `round21` int(11) NOT NULL,
  `round22` int(11) NOT NULL,
  `round23` int(11) NOT NULL,
  `round24` int(11) NOT NULL,
  `round25` int(11) NOT NULL,
  `round26` int(11) NOT NULL,
  `round27` int(11) NOT NULL,
  `round28` int(11) NOT NULL,
  `round29` int(11) NOT NULL,
  `round30` int(11) NOT NULL,
  `round31` int(11) NOT NULL,
  `round32` int(11) NOT NULL,
  `round33` int(11) NOT NULL,
  `round34` int(11) NOT NULL,
  `round35` int(11) NOT NULL,
  `round36` int(11) NOT NULL,
  `round37` int(11) NOT NULL,
  `round38` int(11) NOT NULL,
  `round39` int(11) NOT NULL,
  `round40` int(11) NOT NULL,
  `round41` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `four-in-a-row-game`
--
ALTER TABLE `four-in-a-row-game`
  ADD PRIMARY KEY (`game_ID`),
  ADD KEY `game_ID` (`game_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
