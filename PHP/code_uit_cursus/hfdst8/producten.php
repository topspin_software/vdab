<?php

//producten.php

class Product {

    private $prijs;

    public function __construct($prijs) {
        $this->prijs = $prijs;
    }

    public function getPrijs() {
        return $this->prijs;
    }

    public function setPrijs($prijs) {
        $this->prijs = $prijs;
    }

}

class Dvd extends Product {

    private $speelduur;

    public function __construct($speelduur, $prijs) {
        parent::__construct($prijs);
        $this->setSpeelduur($speelduur);
    }

    public function getSpeelduur() {
        return $this->speelduur;
    }

    public function setSpeelduur($speelduur) {
        $this->speelduur = $speelduur;
    }

}

class Boek extends Product {

    private $auteur;

    public function getAuteur() {
        return $this->auteur;
    }

    public function setAuteur($auteur) {
        $this->auteur = $auteur;
    }

}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Producten</title>
    </head>
    <body>
        <?php
        //objecten aanmaken :
        $product = new Product(50);
        $dvd = new Dvd(60, 12);
        $boek = new Boek(21);
        $boek->setAuteur('J.K. Rowling');
        ?>
    </body>
</html>
