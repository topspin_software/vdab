<?php

//personen.php

class Persoon {

    private $naam;
    private $leeftijd;

    public function __construct($naam, $leeftijd) {
        $this->naam = $naam;
        $this->leeftijd= $leeftijd;
    }

    public function getNaam() {
        return $this->naam;
    }

    public function getLeeftijd() {
        return $this->leeftijd;
    }

    public function setNaam($naam) {
        if ($naam != "") {
            $this->naam = $naam;
        } else {
            $this->naam = "anoniem";
        }
    }

    public function setLeeftijd($leeftijd) {
        if ($leeftijd > 0) {
            $this->leeftijd = $leeftijd;
        } else {
            $this->leeftijd = 0;
        }
    }

}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Personen</title>
    </head>
    <body>
        <?php
        $persoon = new Persoon();
        $persoon->leeftijd = -15; // fout tijdens uitvoering!
        $persoon->setLeeftijd(-15); // geen fout tijdens uitvoering; geen geldige
        // leeftijd; leeftijd= 0!
        $persoon->setLeeftijd(20); // geldige leeftijd; leeftijd= 15
        ?>

    </body>
</html>
