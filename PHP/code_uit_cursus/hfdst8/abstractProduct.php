<?php

//abstractProduct.php

abstract class Product {

    private $prijs;
    private $omschrijving;

    public function getPrijs() {
        return $this->prijs;
    }

    public function getOmschrijving() {
        return $this->omschrijving;
    }

    public function setPrijs($prijs) {
        $this->prijs = $prijs;
    }

    public function setOmschrijving($omschrijving) {
        $this->omschrijving = $omschrijving;
    }

    abstract public function drukAf();
}

class Dvd extends Product {

    private $speelduur;
    private $genre;

    public function getSpeelduur() {
        return $this->speelduur;
    }

    public function getGenre() {
        return $this->genre;
    }

    public function setSpeelduur($speelduur) {
        $this->speelduur = $speelduur;
    }

    public function setGenre($genre) {
        $this->genre = $genre;
    }

    public function drukAf() {
        // Hier komt de code voor het afdrukken van een Dvd
    }

}

class Boek extends Product {

    private $uitgever;
    private $auteur;

    public function getUitgever() {
        return $this->uitgever;
    }

    public function getAuteur() {
        return $this->auteur;
    }

    public function setUitgever($uitgever) {
        $this->uitgever = $uitgever;
    }

    public function setAuteur($auteur) {
        $this->auteur = $auteur;
    }

    public function drukAf() {
        // Hier komt de code voor het afdrukken van een Boek
    }

}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Abstracte Producten</title>
    </head>
    <body>
        <?php
        $b = new Boek();
        $b->setPrijs(25.0);
        print($b->getPrijs());
        ?>

    </body>
</html>
