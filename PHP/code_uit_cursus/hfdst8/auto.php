<?php

//auto.php

class Auto {

    private $kleur;
    private $aantalDeuren;
    private $verbruik;

    public function __construct() {
        $this->kleur= "wit";
        $this->aantalDeuren = 2;
        $this->verbruik= 10;
    }

    public function getKleur() {
        return $this->kleur;
    }

    public function getAantalDeuren() {
        return $this->aantalDeuren;
    }

    public function getVerbruik() {
        return $this->verbruik;
    }

    public function setKleur($eenKleur) {
        $this->kleur = $eenKleur;
    }

    public function setAantalDeuren($aantalDeuren) {
        $this->aantalDeuren = $aantalDeuren;
    }

    public function setVerbruik($eenVerbruik) {
        $this->verbruik = $eenVerbruik;
    }

}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Auto's</title>
    </head>
    <body>
        <?php
        $auto1 = new Auto();
        $auto1->setKleur("rood");
        print("auto1 : " . $auto1->getKleur());
        //print($auto1->kleur); 
        //$auto1->kleur = "wit";
        print('<br />');
        $auto2 = new Auto();
        $auto2->setKleur("groen");
        print("auto2 : " . $auto2->getKleur());
        ?>

        <br />

        <?php
        $kleineAuto = new Auto();
        $groteAuto = new Auto();
        $kleineAuto->setAantalDeuren(3);
        $groteAuto->setAantalDeuren(5);
        $groteAuto->setAantalDeuren($kleineAuto->getAantalDeuren() + 2);
        ?>

        <br />
        <p>Aantal deuren in een kleine auto:
            <?php print($kleineAuto->getAantalDeuren()); ?></p>

        <p>Aantal deuren in een grote auto:
            <?php print($groteAuto->getAantalDeuren()); ?></p>

    </body>
</html>
