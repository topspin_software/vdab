<?php

//helloworld.php

class GreetingGenerator {

    public function getGreeting() {
//        return "Hello world!";
//        return "10/04/2012 - 16:19:48";
        return date("d/m/Y - H:i:s");
    }

}
?>


<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Hello world</title>
    </head>
    <body>
        <h1>
            <?php
            $gg = new GreetingGenerator();
            print($gg->getGreeting());
            ?>
        </h1>

    </body>
</html>
