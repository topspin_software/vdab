<?php

//Business/GenreService.php

require_once("Data/GenreDAO.php");

class GenreService {

    public function getGenresOverzicht() {
        $genreDAO = new GenreDAO();
        $lijst = $genreDAO->getAll();
        return $lijst;
    }

}
