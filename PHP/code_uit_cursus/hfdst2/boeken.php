<?php

//boeken.php

class Boek {

    public function getTitel() {
        $titel = "Handleiding HTML";
        return $titel;
    }
}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Boeken</title>
    </head>
    <body>
        <h1>
            <?php
            $boek = new Boek();
            print($boek->getTitel());
            ?>
        </h1>

    </body>
</html>
