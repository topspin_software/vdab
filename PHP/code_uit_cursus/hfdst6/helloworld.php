<?php

//helloworld.php

class GreetingGenerator {

    public function getGreeting() {
        return "Hello world!";
    }

}
?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Hello world</title>
    </head>
    <body>
        <h1>
            <?php
            $gg = new GreetingGenerator();
            print($gg->getGreeting());
            ?>
        </h1>

    </body>
</html>
