<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>

  <body>
    <?php
    //getallenkiezen.php
      print "Voer 2 getallen in via de URL. Noem ze 'getal1' en 'getal2'. ";
      print "Geef daarna op welke bewerking er moet uitgevoerd worden. ";
      print "Noem de bewerking 'operand'. ";
      $getal1 = $_GET["getal1"];
      $getal2 = $_GET["getal2"];
      $operand = $_GET["operand"];
      switch ($operand) {
        case 1: $result = $getal1 + $getal2; break;        
        case 2: $result = $getal1 - $getal2; break;
        case 3: $result = $getal1 * $getal2; break;
        case 4: $result = $getal1 / $getal2; break;
        default: print "ongeldige bewerking gespecifieerd!!";
      }
      print "RESULTAAT = ";
      print($result);
    ?>
  </body>

</html>
