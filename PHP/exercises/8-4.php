<?php
  class Person {
    private $familyName;
    private $firstName;
    public function getFirstName() {
      return $this->firstName;
    }
    public function getFamilyName() {
      return $this->familyName;
    }
    public function getFullName() {
      return $this->getFamilyName().", ".$this->getFirstName();
    }
    public function setFirstName($par_firstName) {
      $this->firstName = $par_firstName;
    }
    public function setFamilyName($par_familyName) {
      $this->familyName = $par_familyName;
    }
  }
  class Student extends Person {
    public function getFullName() {
      return parent::getFullName()." (student)";
    }
  }
  class Assistant extends Person {
    public function getFullName() {
      return parent::getFullName()." (assistant)";
    }
  }
	$student = new Student();
	$assistant = new Assistant();
	$student->setFamilyName("Peeters");
	$student->setFirstName("Jan");
	$assistant->setFamilyName("Janssens");
	$assistant->setFirstName("Tom");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>oef 8.4 - Students and Assistants</title>
  </head>
  <body>
  	<h1>Names</h1>
		<ul>
			<li><?php print($student->getFullName());?></li>
			<li><?php print($assistant->getFullName());?></li>
		</ul>

  </body>
</html>
