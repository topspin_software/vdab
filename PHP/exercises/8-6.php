<?php
class Account {
  private $accountNumber;
  private $balance;
  private static $interest = 0.03;
  public function __construct($par_accountNumber) {
    $this->setAccountNumber($par_accountNumber);
    $this->balance = 0;
  }
  public function setAccountNumber($par_accountNumber) {
    $this->accountNumber = $par_accountNumber;
  }
  public function getAccountnNumber() {
    return $this->accountNumber;
  }
  public function setBalance($par_balance) {
    $this->balance = $par_balance;
  }
  public function getBalance() {
    return $this->balance;
  }
  public static function setInterest($par_interest) {
    self::$interest = $par_interest;
  }
  public static function getInterest() {
    return self::$interest;
  }
  public function creditAccount($amount) {
    if ($amount < 0) {
      echo "You cannot credit this account with a negative amount! Request cancelled.";
      echo "Please specify only positive amounts.";
      return -1;
    } 
    $tmp_amount = $this->getBalance() + $amount;
    $this->setBalance($tmp_amount);
  }
  public function debetAccount($amount) {
    if ($amount < 0) {
      echo "You cannot debet this account with a negative amount! Request cancelled.";
      echo "Please specify only positive amounts.";
      return -1;
    } 
    $tmp_amount = $this->getBalance() - $amount;
    $this->setBalance($tmp_amount);
  }
  public function processInterest() {
    $tmp_balance = $this->getBalance() * (1 + $this->getInterest());
    $this->setBalance($tmp_balance);
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title></title>
  </head>
  <body>
		<h1>
			<?php
			$account = new Account("091-0122401-16");
			print("Het saldo is: " .$account->getBalance() . "<br />");
			$account->creditAccount(200);
			print("Het saldo is: " .$account->getBalance() . "<br />");
			$account->processInterest();
			print("Het saldo is: " .$account->getBalance() . "<br />");
			?>
		</h1>
  </body>
</html>
