<?php
// Seven Doors
session_start();
class Game {
  public function initialize_game_history() {
    $_SESSION['history'] = array();
    $_SESSION['history'][0] = 0;
    $_SESSION['gameNumber'] = 0;
    $_SESSION['success'] = FALSE;
  }
  public function update_game_history() {
    if (!$_SESSION['success']) {
      $_SESSION['history'][$_SESSION['gameNumber']] = 'opgegeven';
    } else {
    $_SESSION['history'][$_SESSION['gameNumber']] = $_SESSION['attempts'];
    }
  }
  public function list_game_history() {
    for ($i=1; isset($_SESSION['history'][$i]); $i++) {                              
      echo "<p class='newline'>Spel no. ";
      echo $i;
      echo " : ";
      echo $_SESSION['history'][$i];
      switch ($_SESSION['history'][$i]) {
        case 1 : echo " poging</p>";
                 break;
        case 'opgegeven' : break;
        default : echo " pogingen</p>";
      }
    }
  }
  public function initialize_game() {
    $_SESSION['gameNumber']++;
    $_SESSION['attempts'] = 0;    
    $_SESSION['success'] = FALSE;
  }
}
class Door {
  public function drawDoor($index) {
    switch ($_SESSION['doorStatus'][$index]) {
      case 0:  return "doorclosed.png";
      case 1:  return "dooropen.png";
      case 10: return "doorclosed.png";
      case 11: return "doortreasure.png";
    }
  }
  public function drawDoors () {
    for ($i=0; $i<7; $i++) {
      echo "<div class='door'>";
      echo "<a href='7-5.php?door=";
      echo $i;
      echo "'><img alt='deur' src='images/";
      echo $this->drawDoor($i);
      echo "'></a>";
      echo "</div>";
    }
  }
  public function initialize_doors() {
    $_SESSION['doorStatus'] = array(0,0,0,0,0,0,0);
    $gold = mt_rand(0, 6);
    $_SESSION['doorStatus'][$gold] = 10;
    $gold++;
  }
  public function checkOnClick($index) {
    $_SESSION['doorStatus'][$index]++;
    $_SESSION['attempts']++;
    switch ($_SESSION['doorStatus'][$index]) {
      case 2 : $_SESSION['doorStatus'][$index]--;
               break;
      case 11: $_SESSION['success'] = TRUE;
               break;
      default: break;
    }
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>Seven Doors</title>
    <link type='text/css' rel='stylesheet' href='css/exercises.css'/>
  </head>
  <body>
    <h1 class='oef7-5'>Kies een deur</h1>
    <?php
      $game = new Game;
      $door = new Door;
      if (isset($_GET['newgame'])) {
        $_SESSION['newgame'] = $_GET['newgame'];
      }
      if (!isset($_SESSION['history'])) {
        $game->initialize_game_history();
        $_SESSION['newgame'] = TRUE;
      }
      if ($_SESSION['newgame']) {
        $game->update_game_history();
        $game->initialize_game();
        $door->initialize_doors();
        $_SESSION['newgame'] = FALSE;
      }
      if (!$_SESSION['success']) {
        if (isset($_GET['door'])) {
          $door->checkOnClick($_GET['door']);
        }
      }
      $door->drawDoors();
      if ($_SESSION['success']) {
        echo "<p class='newline'>HOERA! U heeft de schat gevonden! En dat na (amper) ".$_SESSION['attempts']." pogingen.</p>";
      } else {
        echo "<p class='newline'>U heeft in dit spel al ";
        echo $_SESSION['attempts'];
        echo " pogingen ondernomen.</p>";
      }
    ?>
    <p class='newline'>Klik <a href='7-5.php?newgame=true'>hier</a> om een nieuw spel te beginnen.</p>
    <h2>Games History for current Player</h2>
    <?php $game->list_game_history(); ?>
  </body>
</html>
