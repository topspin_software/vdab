<?php
  class Quote {
    public function getBlockQuote($fontSize_start, $fontSize_stop, $steps) {
      $fontSize_diff = ($fontSize_stop - $fontSize_start)/$steps;
      for ($i=0; $i<=$steps; $i++) {
        $fontSize[$i] = $fontSize_start + ($i * $fontSize_diff); 
        // print "opstap ".$i." : ".$fontSize[$i]."<br>";
      }
      for ($i; $i<=$steps*2; $i++) {
        $fontSize[$i] = $fontSize[$i-1] - $fontSize_diff;
        // print "afstap ".$i." : ".$fontSize[$i]."<br>";
      }
      return $fontSize;
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>block quotes</title>
    <link href='css/exercises.css' type='text/css' rel='stylesheet'/>
  </head>
  <body>
    <form action='7-1.php' method='post'>
      Font size start: <input type='number' name='start' required>
      Font size stop : <input type='number' name='stop' required>
      Aantal stappen : <input type='number' name='steps' required>
      Quote          : <input type='text' name='quote' required>
      <input type='submit' value='OK'>
    </form>
    <div class='emptyline'></div>
    <div class='blockquote'>
      <?php
      if (isset($_POST['start'])) {  
        $txt_start = $_POST['start'];
        $txt_stop = $_POST['stop'];
        $stappen = $_POST['steps'];
        $text = $_POST['quote'];
        $quote = new Quote;
        $blockQuote = $quote->getBlockQuote($txt_start, $txt_stop, $stappen);
        for ($t=0; isset($blockQuote[$t]); $t++) {
          echo "<p style='font-size:".$blockQuote[$t]."px'>".$text."</p>";
        }
        unset($_POST);
      }
    ?>
    </div>
  </body>
</html>
