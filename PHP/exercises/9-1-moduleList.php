<?php
class ModuleList
{
    private static $dsn = 'mysql: host=localhost;dbname=cursusphp;charset=utf8';
    private static $user = 'cursusgebruiker';
    private static $pwd = 'cursuspwd';
    public function getList($minPrijs, $maxPrijs)
    {
        $dbh = new PDO(self::$dsn, self::$user, self::$pwd);
        $sql = "SELECT naam, prijs FROM modules WHERE ((prijs >= :minPrijs) AND (prijs <= :maxPrijs))";
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':minPrijs', $minPrijs, PDO::PARAM_INT);
        $stmt->bindParam(':maxPrijs', $maxPrijs, PDO::PARAM_INT);
        $stmt->execute();
        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $dbh = null;
        $stmt = null;
        return $resultSet;
    }
}
?>

