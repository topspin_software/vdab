<?php
//random.php
  session_start();
  class RandomNumber {
    public function getRandomNumber($low, $high) {
      if (!isset($_SESSION['random'])) {
        $_SESSION['random'] = mt_rand($low, $high);
      }
      return $_SESSION['random'];
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <form action='5-1.php' method='post'>
      Er moet een random getal gegenereerd worden tussen...<br><br>
      ondergrens: <input type='number' name='minimum' required>
      en
      bovengrens: <input type='number' name='maximum' required> <br><br>
      <input type='submit' value='OK'>
    </form>
    <br><br>
    <?php
      $rnd = new RandomNumber;
      if (isset($_POST['minimum'])) {
        $min = $_POST['minimum'];
        $max = $_POST['maximum'];
        unset($_POST);
        if ($max<$min) {
          print('Bovengrens is kleiner dan ondergrens. Beide getallen worden omgewisseld.');
          print PHP_EOL;
          $tmp = $min;
          $min = $max;
          $max = $tmp;
        }
        $rand = $rnd->getRandomNumber($min, $max);
        print "Het random gegenereerde getal tussen " . $min . " en " . $max . " is: " . $rand;
        unset($_SESSION['random']);
      }
    ?>
  </body>
</html>
