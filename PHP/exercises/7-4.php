<?php
// Lotto
class Lottogetal {
  public function getLottogetallen($count, $min, $max) {
    $lottogetallen = array();
    for ($i=0; $i<$count; $i++) {
      do $index = mt_rand($min, $max);
      while (isset($lottogetallen[$index]));
      $lottogetallen[$index] = 1;
    }
    return $lottogetallen;
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Lotto</title>
    <link href='css/exercises.css' type='text/css' rel='stylesheet'/>
  </head>
  <body>
    <?php
      $min = 1;
      $max = 42;
      $aantal = 6;
      $lottogetal = new Lottogetal;
      $winnendeNummers = $lottogetal->getLottogetallen($aantal, $min, $max);
      $index = 0;
      echo "<table>";
      for ($t=1; $t<=6; $t++) {
        echo "<tr>";
        for ($i=1; $i<=7; $i++) {
          $index += 1;
          echo "<td";
          if (isset($winnendeNummers[$index])) {
            echo " style='background-color: red'";
          }
          echo ">".$index."</td>";
        }
      }
    ?>
  </body>
</html>
