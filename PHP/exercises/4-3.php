<?php
//fibonacci
  class Fibonacci {
    public function generateSequence($count) {
      $fibonacci[0] = 0;
      $fibonacci[1] = $i = 1;
      for ($i=2; $i<$count; $i++) {
        $fibonacci[$i] = $fibonacci[$i-1] + $fibonacci[$i-2];
      }
      return $fibonacci;
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <pre>
    <?php
      $sequence = new Fibonacci;
      $fibo_reeks = $sequence->generateSequence(30);
      print_r($fibo_reeks);
    ?>
    </pre>
  </body>
</html>
