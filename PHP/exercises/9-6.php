<?php
//9-6.php - chatroom
class Message {
    private static $dsn = 'mysql:host=localhost;dbname=cursusphp;charset=utf8';
    private static $usr = 'cursusgebruiker';
    private static $pwd = 'cursuspwd';
    private static $dbh = null;
    private $message;
    private $dateTime;
    private $user;
    private $id;

    public function __construct($message = null, $user = null)
    {
        $this->setMessage($message);
        $this->setUser($user);
        $now = date('Y/m/d H:i:s');
        $this->setTimeStamp($now);
    }
    private static function connectDB()
    {
        self::$dbh = new PDO(self::$dsn, self::$usr, self::$pwd);
    }
    private static function disconnectDB()
    {
        self::$dbh = null;
    }
    public function setMessage($message) {
        $this->message = $message;
    }
    public function setTimeStamp($timeStamp) {
        $this->timeStamp = $timeStamp;
    }
    public function setUser($name) {
        $this->user = $name;
    }
    public function addMessage() {
        $now = date('Y/m/d H:i:s');
        $this->setTimeStamp($now);
        self::connectDB();
        $sql = 'INSERT INTO chatroom (user, message, timestamp) VALUES (:user, :message, :timeStamp)';
        $stmt = self::$dbh->prepare($sql);
        $stmt->bindParam(':user', $this->user);
        $stmt->bindParam(':message', $this->message);
        $stmt->bindParam(':timeStamp', $this->timeStamp);
        $bool = $stmt->execute();
        if ($bool == FALSE) { echo 'execute FAILED'; }
        $this->id = self::$dbh->lastInsertId();
        $stmt = null;
        self::disconnectDB();
    }
    public static function getMessageList()
    {
        self::connectDB();
        $resultSet = self::$dbh->query('SELECT * FROM chatroom ORDER BY timestamp');
        self::disconnectDB();
        return $resultSet;
    }
}

//generic code
if (isset($_GET['action']) && $_GET['action'] == 'newMsg')
{
    $msg = filter_input(INPUT_POST, 'msg', FILTER_SANITIZE_STRING);
    $user = filter_input(INPUT_POST, 'user', FILTER_SANITIZE_STRING);
    $newMsg = new Message($msg, $user);
    $newMsg->addMessage();
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>Gastenboek</title>
    <link rel='stylesheet' type='text/css' href='css/exercises.css'/>
  </head>
  <body>
    <div class='formborder'>
      <h2>Chatroom</h2>
      <?php
      $entryList = Message::getMessageList();
      foreach ($entryList as $row)
      {
          echo '<p>'.$row['user'].' ('.$row['timestamp'].')</p>';
          echo '<p>'.$row['message'].'</p>';     
          echo '<p class=underlined></p>';
      }
      ?>
    </div>
    <div class='formborder'>
      <h2>Message:</h2>
        <form action='9-6.php?action=newMsg' method='post' class='halfwidth'>
          Naam:<br/>
          <input type='text' name='user' autocomplete='off' required><br/>
          Boodschap:<br/>
          <textarea rows='4' cols='80' name='msg' placeholder='Type Here'></textarea><br/>         
          <input type='submit' value='Submit'>
        </form>
    </div>      
  </body>
</html>

