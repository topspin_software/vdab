<?php
//tafels.php
class Table {
  public function getTable($base) {
    $table=array();
    for ($i=1; $i<=10; $i++) {
      $table[$i]=$base*$i;
    }
    return $table;
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>tafels</title>
    <link rel='stylesheet' type='text/css' href='css/exercises.css'/>
  </head>
  <body>
    <?php
    $baseNumber = $_POST['grondtal'];
    print "<h1>De tafel van $baseNumber</h1>";
      $tafel = new Table;
      $tafelVan = $tafel->getTable($baseNumber);
      for ($t=1; $t<=10; $t++) {
        print "<p>".str_pad($t, 2, '0', STR_PAD_LEFT)." maal $baseNumber = ".str_pad($tafelVan[$t], 3, '0', STR_PAD_LEFT);
      }
    ?>
    <div class='emptyline'></div>
    <div class='buttonLink'>
      <a href='7-2-tafelsForm.php'>BACK</a>
    </div>
  </body>
</html>
