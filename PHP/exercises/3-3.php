<?php
//gokautomaat
  class GokGetal {
    public function kiesGetal($min, $max) {
      return mt_rand($min, $max);
    }
  }
?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <?php
      print "Kies een willekeurig getal tussen 1 en 10 via de URL. Noem het 'gok'. ";
      $getal = new GokGetal;
      $juisteGetal = $getal->kiesGetal(1,10);
      $gok = $_GET["gok"];
      if ($juisteGetal == $gok) {
        print "PROFICIAT! U heeft gewonnen.";
      }
      else {
        print "Helaas. Het juiste getal was ";
        print($juisteGetal);
      }
    ?>
  </body>
</html>
