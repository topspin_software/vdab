<!-- 9-1.php : gegevensOphalenForm -->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>9-1: Modules</title>
    <link rel='stylesheet' type='text/css' href='css/exercises.css'/>
  </head>
  <body>
    <div class='formborder'>
      <h2>Modules</h2>
      <form action='9-1-findModules.php' method='post'>
        <label for='minimumprijs'>Geef een minimumprijs</label>
        <input type='number' name='minimumprijs'> euro<br><br>
        <label for='maximumprijs'>Geef een maximumprijs</label>
        <input type='number' name='maximumprijs'> euro<br><br>
        <input type='submit' value='OK'>
      </form>
    </div>
  </body>
</html>
