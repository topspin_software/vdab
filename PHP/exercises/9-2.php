<?php
class Movie
{
    private $movieID;
    private $movieTitle;
    private $movieRuntime;
    public function __construct($title, $runtime)
    {
        $this->movieTitle = $title;
        $this->movieRuntime = $runtime;
    }
    public static function getMovieList()
    {
        static $dsn = 'mysql:host=localhost;dbname=cursusphp;charset=utf8';
        static $usr = 'cursusgebruiker';
        static $pwd = 'cursuspwd';
        $dbh = new PDO($dsn, $usr, $pwd);
        $resultSet = $dbh->query('SELECT * FROM films');
        $dbh = null;
        return $resultSet;
    }
    public function addMovie()
    {
        static $dsn = 'mysql:host=localhost;dbname=cursusphp;charset=utf8';
        static $usr = 'cursusgebruiker';
        static $pwd = 'cursuspwd';
        $dbh = new PDO($dsn, $usr, $pwd);
        $sql = 'INSERT INTO films (titel, duurtijd) VALUES (:title, :runtime)';
        $stmt = $dbh->prepare($sql);
        $stmt->bindParam(':title', $this->movieTitle);
        $stmt->bindParam(':runtime', $this->movieRuntime);
        $bool = $stmt->execute();
        if ($bool == FALSE) { echo 'execute FAILED'; }
        $this->movieID = $dbh->lastInsertId();
        $stmt = null;
        $dbh = null;
    }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>Film List</title>
    <link rel='stylesheet' type='text/css' href='css/exercises.css'/>
  </head>
  <body>
    <div class='formborder'>
      <h2>Alle Films</h2>
      <?php
      if (isset($_POST['title']))
      {
          $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
          $runtime = abs(filter_input(INPUT_POST, 'runtime', FILTER_VALIDATE_INT));
          unset($_POST['title']);
          unset($_POST['runtime']);
          $movie = new Movie($title, $runtime);
          $movie->addMovie();
      }
      $movieList = Movie::getMovieList();
      echo '<lu>';
      foreach ($movieList as $row)
      {
          echo '<li>'.$row['titel'].' ('.$row['duurtijd'].' min)</li>';
      }
      echo '</lu>';   
      ?>
    </div>
    <br>
    <div class='formborder'>
      <h2>Film toevoegen</h2>
        <form action='9-2.php' method='post'>
          Titel: <input type='text' name='title' autocomplete='off' required>
          Duurtijd: <input type='number' name='runtime'>
          <input type='submit' value='OK'>
      </form>
    </div>      
  </body>
</html>
