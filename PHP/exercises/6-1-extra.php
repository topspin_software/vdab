<?php
// genereert een reeks getallen adhv. input: aantal, minimum- en maximumwaarde
class RandomNumberSequence {
  public function getRandomNumberSequence($count, $min, $max) {
    if ($min > $max) {
      $tmp = $min;
      $min = $max;
      $max = $tmp;
    }
    $randomNumberSequence=array();
    for ($i=0; $i<$count; $i++) {
      $randomNumberSequence[] = mt_rand($min, $max);
    }
    return $randomNumberSequence;
  }
}
?>
<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>

  <body>
    <form action='6-1-extra.php' method='post'>
      Hoeveel getallen moeten er gegenereerd worden?<br>
      <input type='number' name='aantal' required><br>
      Welke zijn de minimum- en maximumwaarden?<br>
      <input type='number' name='getal1' required><br>
      <input type='number' name='getal2' required><br>
      Hoe wil je deze reeks sorteren?<br>
      <select name='volgorde' required>
        <option selected value='oplopend'>oplopend</option>
        <option value='aflopend'>aflopend</option>
      </select>
      <input type='submit' value='OK'><br>
    </form>
    <?php
      $form_filled = (isset($_POST['aantal']));
      if ($form_filled) {
        $getal1 = $_POST['getal1'];
        $getal2 = $_POST['getal2'];
        $aantal = $_POST['aantal'];
        $reeks = new RandomNumberSequence;
        $randomReeks = $reeks->getRandomNumberSequence($aantal, $getal1, $getal2);
        echo "<p>De volgende reeks werd gegenereerd:</p>";
        foreach($randomReeks as $key => $value) {
          echo "<span>Het ".$key."e getal is: $value ".PHP_EOL."</span><br>";
        }
        $direction = $_POST['volgorde'];
        (($direction == 'oplopend') ? sort($randomReeks) : rsort($randomReeks));
        echo '<p>Dit is de gesorteerde reeks: </p>';
        foreach ($randomReeks as $key => $value) {
          echo '<span>Het '.$key.'e getal is: '.$value.'</span><br>';
        }
      }
    ?>
  </body>

</html>
