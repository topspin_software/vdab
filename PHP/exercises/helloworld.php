<?php
// helloworld.php
 
class GreetingGenerator {
  public function getGreeting() {
    return date("d/m/Y - H:i:s");
  }
}
?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
  <head>
    <title>Hello world</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <h1>
      <?php
      $gg = new GreetingGenerator();
      print($gg->getGreeting());
      ?>
    </h1>
  </body>
</html>
