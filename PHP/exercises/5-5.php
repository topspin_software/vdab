  <?php
  session_start();
  $select = array();
  $select['white'] = $select['red'] = $select['green'] = $select['blue'] = $select['purple'] = $select['gray'] = $select['antiquewhite'] = $select['orange'] = '';
  if (isset($_POST['color'])) {
    $bg_color = $_POST['color'];
    setcookie('bgcolor', $bg_color, time() + 86400);
  }
  elseif (isset($_COOKIE['bgcolor'])) {
    $bg_color = $_COOKIE['bgcolor'];
  }
  else { $bg_color = 'white'; }
  $select[$bg_color] = 'selected';
  // print_r($select);
  ?>
<!DOCTYPE html>
<html>

  <head>
    <meta charset='UTF-8'>
    <title>kleur.php</title>
    <link rel='stylesheet' type='text/css' href='css/exercises.css'/>
  </head>

  <body style='background-color:<?php echo $bg_color ?>'>
    <form action='5-5.php' method='post' class='colorChoiceForm'>
      <span>Kies uw favoriete achtergrondkleur: </span>
      <select name='color' size='1'>
        <option <?php echo $select['white']; ?> value='white'>white</option>
        <option <?php echo $select['red']; ?> value='red'>red</option>
        <option <?php echo $select['green']; ?> value='green'>green</option>
        <option <?php echo $select['blue']; ?> value='blue'>blue</option>
        <option <?php echo $select['purple']; ?> value='purple'>purple</option>
        <option <?php echo $select['gray']; ?> value='gray'>gray</option>
        <option <?php echo $select['antiquewhite']; ?> value='antiquewhite'>antiquewhite</option>
        <option <?php echo $select['orange']; ?> value='orange'>orange</option>
      </select>
      <input type='submit' value='OK'><br>
      <a href='5-5.php'>pagina vernieuwen</a>
    </form>
  </body>

</html>
