<?php
  class NumberSequence {
    public function generateNumbers($count, $first) {
      $sequence = array();
      $sequence[0] = $first;
      for ($i=1; $i<$count; $i++) {
        $sequence[$i] = $sequence[$i-1] + $i;
      }
      return $sequence;
    }
  }

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <pre>
      <?php
      $reeks = new NumberSequence;
      print "een reeks van 50 getallen waarvan waardeverschil met voorganger telkens 1 verhoogt." . PHP_EOL;
      print_r($reeks->generateNumbers(50, 0));
      ?>
    </pre>
  </body>
</html>
