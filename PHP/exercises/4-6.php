<?php
//split_push.php
class OddEvenArray {
  public function getOddEvenArray($min, $max) {
    // number of elements to put in the array
    $count = $max - $min + 1;
    // calculate at which index the even numbers must start; depends on begin- and endnumber being even and/or odd
    $evenStart = intval($count/2);
    if (($min%2) && ($max%2)) { $evenStart += 1; }
    // assign key & value
    for ($i=0; $i<$count; $i++) {
      $index = intval($i/2);
      if (($min+$i)%2) { $reeks[$index] = $min + $i; }
      else { $reeks[$evenStart + $index] = $min + $i; }
    }  
    return $reeks;
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title></title>
  </head>
  <body>
    <form action='4-6.php' method='post'>
      Begingetal: <input type='number' name='begingetal'>
      Eindgetal: <input type='number' name='eindgetal'>
      <input type='submit' value='OK'>
    </form>
    <pre>
      <?php
      // initialize vars to avoid error msgs when waiting for form input
      $beginGetal = $eindGetal = 0;
      $splitArray = new OddEvenArray;
      $beginGetal = $_POST['begingetal'];
      $eindGetal = $_POST['eindgetal'];
      $tabel = $splitArray->getOddEvenArray($beginGetal, $eindGetal);
      // note that key is NOT the array's index
      print_r($tabel);
      // need for-loop to print the array in order of the keys
      $aantal = $eindGetal - $beginGetal + 1;
      for ($t=0; $t<$aantal; $t++) {
        print "[" . $t . "] =>  ";
        print($tabel[$t]) . PHP_EOL;
      }
      ?>
    </pre>
  </body>
</html>
