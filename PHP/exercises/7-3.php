<?php
  class MultiplicationTable {
    public function getMultiplicationTable($min, $max) {
      $table=array();
      for ($f1=$min; $f1<=$max; $f1++) {
        $table[$f1][0] = $f1;
        for ($f2=$min; $f2<=$max; $f2++) {
          $table[$f1][$f2] = $f1*$f2;
          $table[0][$f2] = $f2;
        }
      }
      return $table;
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>7-3-multiplication tables</title>
    <link href='css/exercises.css' type='text/css' rel='stylesheet'/>
  </head>
  <body>
    <form action='7-3.php' method='post'>
      <label for='getal1'>Eerste factor: </label>
      <input type='number' name='getal1' id='getal1' required/>
      <label for='getal2'>Tweede factor: </label>
      <input type='number' name='getal2' id='getal2' required/>
      <input type='submit' value='OK'>
    </form>
    <div class='emptyline'></div>
    <?php
    $factor1 = $_POST['getal1'];
    $factor2 = $_POST['getal2'];
    if ($factor2 < $factor1) {
      $tmp = $factor1;
      $factor1 = $factor2;
      $factor2 = $tmp;
    }
    $multiplicationTable = new MultiplicationTable;
    $matrix = $multiplicationTable->getMultiplicationTable($factor1, $factor2);
        print "<div class='cell toprow'></div>";
    for ($s=$factor1; $s<=$factor2; $s++) {
      print "<div class='cell toprow'>$s</div>";
    }
    for ($t1=$factor1; $t1<=$factor2; $t1++) {
      print "<div class='cell header'>$t1</div>";
      for ($t2=$factor1; $t2<=$factor2; $t2++) {   
        print "<div class='cell table'>".$matrix[$t1][$t2]."</div>";
      }
    }
    ?>
  </body>
</html>
