<?php
// Seven Matches
session_start();
class Game {
  public function initializeGameHistory() {
    $_SESSION['history'] = array();
    $_SESSION['history'][0] = 0;
    $_SESSION['gameNumber'] = 0;
  }
  public function updateGameHistory($player) {
    $_SESSION['history'][$_SESSION['gameNumber']] = $player;
  }
  public function listGameHistory() {
    for ($i=1; isset($_SESSION['history'][$i]); $i++) {                              
      echo "<p class='newline'>Spel no. ";
      echo $i;
      echo " : verloren door speler no. ";
      echo $_SESSION['history'][$i];
      echo "</p>";
    }
  }
  public function initializeGame() {
    $_SESSION['gameNumber']++;
    $_SESSION['gameFinished'] = FALSE;
    $_SESSION['remainingMatches'] = abs($_POST['numberOfMatches']);
    $_SESSION['currentPlayer'] = $_POST['playsFirst'];
    $_SESSION['newgame'] = FALSE;
    $_SESSION['gameStarted'] = TRUE;
  }
  public function displayOptionsForm() {
    echo "<form action='7-6.php' method='post'>";
    echo "Aantal lucifers : <input type='number' name='numberOfMatches' required>";
    echo "<br>";
    echo "Wie begint? <select name='playsFirst' size=1 required>";
    echo "<option value='1' selected>Speler 1</option>";
    echo "<option value='2'>Speler 2</option>";
    echo "<input type='submit' value='Begin' name='optionsSubmit'>";
    echo "</form>";
    $_SESSION['gameFinished'] = FALSE;
    $_SESSION['gameStarted'] = FALSE;
  }
  public function displayRemovalButtons() {
    echo "<form action='7-6.php' method='get'>";
    echo "<button class='removalButtons' type='submit' name='removeMatches' value='1'>Eén lucifer wegnemen</button>";
    echo "<button class='removalButtons' type='submit' name='removeMatches' value='2'>Twee lucifers wegnemen</button>";
    echo "</form>";
  }
}
class Match {
  public function drawMatches() {
    for ($i=0; $i<$_SESSION['remainingMatches']; $i++) {
      echo "<div class='match'>";
      echo "<a href='7-6.php?match=";
      echo $i;
      echo "'><img src='images/lucifer.png'/>";
      echo "</a>";
      echo "</div>";
    }
    echo "<div class='newline'/>";
    $_SESSION['matchesDrawn'] = TRUE;
  }
  public function processMatchRemoval() {
    $_SESSION['remainingMatches'] -= $_GET['removeMatches'];
    if ($_SESSION['remainingMatches'] <= 0) {
      $_SESSION['gameFinished'] = TRUE;
    } else {
      $this->switchPlayer();
      }
  }
  public function switchPlayer() {
    switch ($_SESSION['currentPlayer']) {
      case 1 : $_SESSION['currentPlayer'] = 2;
               break;
      case 2 : $_SESSION['currentPlayer'] = 1;
               break;
    }
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>Seven Matches</title>
    <link type='text/css' rel='stylesheet' href='css/exercises.css'/>
  </head>
  <body>
    <h1 class='oef7-5'>Luciferspel</h1>
    <?php
      $game = new Game;
      $match = new Match;
      if (isset($_GET['newgame'])) {
        $_SESSION['newgame'] = $_GET['newgame'];
      }
      if (!isset($_SESSION['history'])) {
        $game->initializeGameHistory();
        $_SESSION['newgame'] = TRUE;
      }
      if ($_SESSION['newgame']) {
        $game->displayOptionsForm();
        if (isset($_POST['optionsSubmit'])) {
          $game->initializeGame();
        }
      }
      if (isset($_GET['removeMatches'])) {
        $match->processMatchRemoval();
      }
      if (isset($_SESSION['remainingMatches'])) {
        $match->drawMatches();
        echo "game number : ".$_SESSION['gameNumber']."<br>";
        echo "remaining matches : ".$_SESSION['remainingMatches']."<br>";
        echo "current player : ".$_SESSION['currentPlayer']."<br>";
      }
      if ($_SESSION['gameFinished']) {
        echo "<p class='newline'>Helaas, u heeft de laatste lucifer(s) weggenomen en verliest dus dit spel!</p>";
        $game->updateGameHistory($_SESSION['currentPlayer']);
      }
      if ((!$_SESSION['gameFinished']) AND ($_SESSION['gameStarted'])) {
        $game->displayRemovalButtons();
      }
    ?>
    <p class='newline'>Klik <a href='7-6.php?newgame=true'>hier</a> om een nieuw spel te beginnen.</p>
    <h2>Games History</h2>
    <?php $game->listGameHistory(); ?>
  </body>
</html>
