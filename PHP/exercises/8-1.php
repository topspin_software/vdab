<?php
  class Thermometer {
    private $currentTemperature;
    public function getCurrentTemperature() {
      return $this->currentTemperature;
    }
    public function setCurrentTemperature($degreesCelsius) {
      $this->currentTemperature = $degreesCelsius;
    }
    public function raiseTemperature($degreesCelsius) {
      $this->currentTemperature += abs($degreesCelsius);
    }
    public function lowerTemperature ($degreesCelsius) {
      $this->currentTemperature -= abs($degreesCelsius);
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <?php
      $thermometer = new Thermometer;
      $thermometer->setCurrentTemperature(22.5);
      echo "De huidige temperatuur bedraagt: ";
      echo $thermometer->getCurrentTemperature();
      echo " graden Celsius<br><br>";
    ?>
    <form action='8-1.php' method='post'>
      Wat is het temperatuurverschil (in graden Celsius)? <input type='number' name='tempDiff'><br>
      Is het warmer of kouder?
      <input type='radio' name='upDown' value='warmer'> warmer
      <input type='radio' name='upDown' value='colder'> kouder
      <input type='submit' value='Submit' name='tempChange'>
    </form>
    <br>
    <?php
      if (isset($_POST['tempChange'])) {
        switch ($_POST['upDown']) {
          case 'warmer' : $thermometer->raiseTemperature($_POST['tempDiff']);
                          break;
          case 'colder' : $thermometer->lowerTemperature($_POST['tempDiff']);
                          break;
        }
      }
      $newTemperature = $thermometer->getCurrentTemperature();
      echo "De huidige (nieuwe) temperatuur bedraagt: ";
      echo $thermometer->getCurrentTemperature();
      echo " graden Celsius<br>";      
    ?>
  </body>
</html>
