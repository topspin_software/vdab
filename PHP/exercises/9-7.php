<?php
// 9-7.php - Four-In-A-Row

/*******************************************************************************
**  CLASS CODE
*******************************************************************************/

abstract class Session {
    private static $empty = NULL;
    
    public static function &get($key) {
    array_key_exists($key, $_SESSION)? $_SESSION[$key] : self::$empty;
    }
}

class Player {
    private $username;
    private $wins = 0;
    private $losses = 0;
    private $draws = 0;
    // array of gameID's - allows link between players and played games
    private $games = array();
    
    public function __construct($name) {
        $this->setUsername($name);
    }
    function getUsername() {
        return $this->username;
    }
    function getWins() {
        return $this->wins;
    }
    function getLosses() {
        return $this->losses;
    }
    function getDraws() {
        return $this->draws;
    }
    function getMatches() {
        return $this->matches;
    }
    function setUsername($username) {
        $this->username = $username;
    }
    function setWins($wins) {
        $this->wins = $wins;
    }
    function setLosses($losses) {
        $this->losses = $losses;
    }
    function setDraws($draws) {
        $this->draws = $draws;
    }
    function setMatches($matches) {
        $this->matches = $matches;
    }
}

class GameBoard {
    private $position = array();
    // 'status' 0 means EMPTY
    // 'status' 1 means YELLOW
    // 'status' -1 means RED
    // e.g. $this->position[3][5] = 1 means "put yellow coin on column 3, row 5
    // columns are counted from left to right 0 -> 6
    // rows are counted from bottom to top 0 -> 5
    function __construct() {
        for ($i=0; $i<7; $i++) {
            for ($j=0; $j<6; $j++) {
                $this->position[$i][$j] = 0;
            }
        }
    }
    function getPosition($column, $row) {
        return $this->position[$column][$row];
    }
    function setPosition($column, $row, $status) {
        $this->position[$column][$row] = $status;
    }
}

class Game {
    // unique Game identifier. Allows links between players and played games
    private $gameID;
    // player1 is ALWAYS the player that starts, i.e. makes the first move
    // $player1 and $player2 are objects of class Player
    private $player1;
    private $color1;  // color for player1. Same deal: 1 = yellow, -1 = red
    private $player2; // $color2 = - $color1
    // $moves is a 1-dim array where index = turn# + 1 and value is column played
    // e.g. $this->moves[2] = 4 means "in round 3, a coin was dropped in column 5"
    //      because we know player1 always starts, and what color he playes...
    //      $this->moves[2] = 4 means:
    //      "in round 3, player2 dropped $color2 coin in column 5"
    private $moves = array();   
}

/*******************************************************************************
**  PROCEDURAL CODE
*******************************************************************************/

if (session_status() == PHP_SESSION_ACTIVE) { // session has started
    $gameID = &Session::get('gameID');
    if ($gameID) {  // game has started
        header('Location: 9-7-game.php');  // session and game started -> game page
    } else {
        header('Location: 9-7-logged_in.php'); // session but no game started -> logged_in page
    }
} else {
    // no session has been started -> we are on the login screen
    header('Location: 9-7-login.php');
}
