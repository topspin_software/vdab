<?php
//analyse.php

class Oefening {
  public function getAnalyse ($getal1, $getal2) {
    $groter = ($getal1 > $getal2);
    if ($groter) {
      print("Het eerste getal is groter dan het tweede");
    }
    else {
      print("Het eerste getal is niet groter dan het tweede");
    }
  }
}
?>

<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8">
    <title>oefening 2.7 - analyse</title>
  </head>

  <body>
    <h1>
      <?php
      $oef = new Oefening;
      print($oef->getAnalyse(12, 12));
      ?>
    </h1>
  </body>

</html>
