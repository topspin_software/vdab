<?php

abstract class DAO {
    private static $dsn = 'mysql:host=localhost;dbname=four-in-a-row;charset=utf8';
    private static $usr = '4admin';
    private static $pwd = '4admin2016';
    private static $dbh = null;
    
    private static function connectDB()
    {
        self::$dbh = new PDO(self::$dsn, self::$usr, self::$pwd);
    }
    private static function disconnectDB()
    {
        self::$dbh = null;
    }
    public function checkLogin($usr, $pwd) {
        self::connectDB();
        $sql = 'SELECT username, password FROM players WHERE username = :usr';
        $stmt = self::$dbh->prepare($sql);
        $stmt->bindParam(':usr', $usr);
        $bool = $stmt->execute();
        if (!$bool) {
            echo 'DB login check FAILED!!!';
            exit(0); // TO BE REMOVED/EDITED
        } else {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($result['username'] == $usr) {
                if ($result['password'] == $pwd) {
                    return array(TRUE, 'Great Success!');
                } else {
                    return array(FALSE, 'password incorrect');
                }                
            } else {
                return array(FALSE, 'username not found');            
            }
        }
        $stmt = null;
        self::disconnectDB();
    }
}
