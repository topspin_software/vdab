<?php
// Rekenmachine.php
 
class Rekenmachine {
  // berekent het kwadraat van een meegegeven getal
  public function getKwadraat($getal) {
    $kwad = $getal * $getal;
    return $kwad;
  }
  /*
  berekent de som van twee meegegeven getallen.
  Dit is een tweede zelfgeschreven functie.
  */
  public function getSom($getal1, $getal2) {
    $resultaat = $getal1 + $getal2;
    return $resultaat;
  }
  // vermenigvuldiging van twee getallen
  public function getProduct($getal1, $getal2) {
    $product = $getal1 * $getal2;
    return $product;
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>oefening 1.3 - rekenmachine</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel='stylesheet' type='text/css' href='css/exercises.css'/>
  </head>
  <body>
    <h1>
      <?php
      $reken = new Rekenmachine();
      print($reken->getKwadraat(5));
      ?>
    </h1>
    <h1>
      <?php
      print($reken->getSom(65, 8));
      ?>
    </h1>
    <h1>
      <?php
      print($reken->getSom(34, 55));
      ?>
    </h1>
    <h1>
      <?php
      print($reken->getProduct(18, 13));
      ?>
    </h1>
  </body>
</html>
