<?php
//gokautomaat
  class GokGetal {
    public function kiesGetal($min, $max) {
      return mt_rand($min, $max);
    }
  }
?>
<!DOCTYPE html>

<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <form action='3-5.php' method='get'>
      Uw gok (getal van 1 tot 10): <input type='number' name='gok'>
      <input type='submit' value='Waag uw kans!'>
    </form>
    <?php
      $getal = new GokGetal;
      $juisteGetal = $getal->kiesGetal(1,10);
      $gok = $_GET["gok"];
      if ($juisteGetal == $gok) {
        print "PROFICIAT! U heeft gewonnen.";
      }
      else {
        print "Helaas. Het juiste getal was ";
        print($juisteGetal);
      }
    ?>
  </body>
</html>
