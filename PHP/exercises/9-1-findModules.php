<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>9-1: findModules</title>
    <link rel='stylesheet' type='text/css' href='css/exercises.css'/>    
  </head>
  <body>
    <div class='formborder'>
      <h2>Zoekresultaat</h2>
      <?php
          //9-1-findModules.php
          include '9-1-moduleList.php';
          $minimumPrijs = filter_input(INPUT_POST, 'minimumprijs', FILTER_VALIDATE_INT);
          $maximumPrijs = filter_input(INPUT_POST, 'maximumprijs', FILTER_VALIDATE_INT);
          $moduleList = new ModuleList;
          $result = $moduleList->getList($minimumPrijs, $maximumPrijs);
          echo "<h3>Modules met prijs tussen ";
          echo "€".$minimumPrijs." en €".$maximumPrijs."</h3>";
          echo "<ul>";
          foreach ($result as $row) {
              echo "<li>".$row['naam']." (".$row['prijs']." euro)".PHP_EOL."</li>";
          }
          echo "</ul>";
      ?>
    </div>
  </body>
</html>

