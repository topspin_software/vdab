<?php
//class related code
class Movie
{
    private static $dsn = 'mysql:host=localhost;dbname=cursusphp;charset=utf8';
    private static $usr = 'cursusgebruiker';
    private static $pwd = 'cursuspwd';
    private static $dbh = null;
    private $movieID = null;
    private $movieTitle = null;
    private $movieRuntime = null;
    public function __construct($title = null, $runtime = null)
    {
        $this->movieTitle = $title;
        $this->movieRuntime = $runtime;
    }
    private static function connectDB()
    {
        self::$dbh = new PDO(self::$dsn, self::$usr, self::$pwd);
    }
    private static function disconnectDB()
    {
        self::$dbh = null;
    }
    public static function getMovieList()
    {
        self::connectDB();
        $resultSet = self::$dbh->query('SELECT * FROM films');
        self::disconnectDB();
        return $resultSet;
    }
    public function addMovie()
    {
        self::connectDB();
        $sql = 'INSERT INTO films (titel, duurtijd) VALUES (:title, :runtime)';
        $stmt = self::$dbh->prepare($sql);
        $stmt->bindParam(':title', $this->movieTitle);
        $stmt->bindParam(':runtime', $this->movieRuntime);
        $bool = $stmt->execute();
        if ($bool == FALSE) { echo 'execute FAILED'; }
        $this->movieID = self::$dbh->lastInsertId();
        $stmt = null;
        self::disconnectDB();
    }
    public static function eraseMovie($id)
    {
        self::connectDB();
        $sql = 'DELETE FROM films WHERE id = :id';
        $stmt = self::$dbh->prepare($sql);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        self::disconnectDB();
        $stmt = null;     
    }
}

//generic code
if (isset($_GET['action']) && ($_GET['action'] == 'verwijder'))
{
    $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
    Movie::eraseMovie($id);
}
if (isset($_POST['title']))
{
    $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
    $runtime = abs(filter_input(INPUT_POST, 'runtime', FILTER_VALIDATE_INT));
    unset($_POST['title']);
    unset($_POST['runtime']);
    $movie = new Movie($title, $runtime);
    $movie->addMovie();
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>Film List</title>
    <link rel='stylesheet' type='text/css' href='css/exercises.css'/>
  </head>
  <body>
    <div class='formborder'>
      <h2>Alle Films</h2>
      <?php
      $movieList = Movie::getMovieList();
      echo '<lu class=movielist>';
      foreach ($movieList as $row)
      {
          echo '<li><a href="9-3.php?action=verwijder&id=';
          echo $row['id'];
          echo '"><img src=images/delete.png></a> ';
          echo $row['titel'].' ('.$row['duurtijd'].' min)</li>';
      }
      echo '</lu>';   
      ?>
    </div>
    <div class='formborder'>
      <h2>Film toevoegen</h2>
        <form action='9-3.php' method='post'>
          Titel: <input type='text' name='title' autocomplete='off' required>
          Duurtijd: <input type='number' name='runtime'>
          <input type='submit' value='OK'>
      </form>
    </div>      
  </body>
</html>
