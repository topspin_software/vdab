<?php
// Rekenmachine.php
class Rekenmachine {
  // berekent het kwadraat van een meegegeven getal
  public function getKwadraat($getal) {
    $kwad = $getal * $getal;
    return $kwad;
  }
  /*
  berekent de som van twee meegegeven getallen.
  Dit is een tweede zelfgeschreven functie.
  */
  public function getSom($getal1, $getal2) {
    $resultaat = $getal1 + $getal2;
    return $resultaat;
  }
}
?>
<!DOCTYPE html>

<html>
  <head>
    <title>oefening 1.1 - Rekenmachine</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>
  <body>
    <h1>
      <?php
      $reken = new Rekenmachine();
      print($reken->getKwadraat(5));
      ?>
    </h1>
    <h1>
      <?php
      print($reken->getSom(65, 8));
      ?>
    </h1>
    <h1>
      <?php
      print($reken->getSom(34, 55));
      ?>
    </h1>
  </body>
</html>
