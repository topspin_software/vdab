<?php
//9-5.php - gastenboek
class Entry {
    private static $dsn = 'mysql:host=localhost;dbname=cursusphp;charset=utf8';
    private static $usr = 'cursusgebruiker';
    private static $pwd = 'cursuspwd';
    private static $dbh = null;
    private $message;
    private $dateTime;
    private $author;
    private $id;

    public function __construct($message = null, $author = null)
    {
        $this->setMessage($message);
        $this->setAuthor($author);
        $now = date('Y/m/d H:i:s');
        $this->setDateTime($now);
    }
    private static function connectDB()
    {
        self::$dbh = new PDO(self::$dsn, self::$usr, self::$pwd);
    }
    private static function disconnectDB()
    {
        self::$dbh = null;
    }
    public function setMessage($message) {
        $this->message = $message;
    }
    public function setDateTime($dateTime) {
        $this->dateTime = $dateTime;
    }
    public function setAuthor($name) {
        $this->author = $name;
    }
    public function addEntry() {
        $now = date('Y/m/d H:i:s');
        $this->setDateTime($now);
        self::connectDB();
        $sql = 'INSERT INTO gastenboek (auteur, boodschap, datum) VALUES (:author, :message, :date)';
        $stmt = self::$dbh->prepare($sql);
        $stmt->bindParam(':author', $this->author);
        $stmt->bindParam(':message', $this->message);
        $stmt->bindParam(':date', $this->dateTime);
        $bool = $stmt->execute();
        if ($bool == FALSE) { echo 'execute FAILED'; }
        $this->id = self::$dbh->lastInsertId();
        $stmt = null;
        self::disconnectDB();
    }
    public static function getEntryList()
    {
        self::connectDB();
        $resultSet = self::$dbh->query('SELECT * FROM gastenboek ORDER BY datum DESC');
        self::disconnectDB();
        return $resultSet;
    }
}

//generic code
if (isset($_GET['action']) && $_GET['action'] == 'newEntry')
{
    $msg = filter_input(INPUT_POST, 'msg', FILTER_SANITIZE_STRING);
    $author = filter_input(INPUT_POST, 'author', FILTER_SANITIZE_STRING);
    $newEntry = new Entry($msg, $author);
    $newEntry->addEntry();
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>Gastenboek</title>
    <link rel='stylesheet' type='text/css' href='css/exercises.css'/>
  </head>
  <body>
    <div class='formborder'>
      <h2>Gastenboek</h2>
      <?php
      $entryList = Entry::getEntryList();
      foreach ($entryList as $row)
      {
          echo '<p>'.$row['auteur'].' ('.$row['datum'].')</p>';
          echo '<p>'.$row['boodschap'].'</p>';     
          echo '<p class=underlined></p>';
      }
      ?>
    </div>
    <div class='formborder'>
      <h2>Boodschap toevoegen</h2>
        <form action='9-5.php?action=newEntry' method='post' class='halfwidth'>
          Naam: <br/>
          <input type='text' name='author' autocomplete='off' required> <br/>
          Boodschap: <br/>
          <input type='text' name='msg' maxlength='200'>
          <input type='submit' value='OK'>
      </form>
    </div>      
  </body>
</html>

