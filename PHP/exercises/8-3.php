<?php
  session_start();
  class Thermometer {
    private $currentTemperature;
    public function __construct($startingTemp) {
      $this->currentTemperature = $startingTemp;
      // echo "(DEBUG construct) Current temperature is:     ".$this->currentTemperature."<br>"; //DEBUG
      // echo "(DEBUG construct) Session temperature var is: ".$_SESSION['currentTemperature']."<br>"; //DEBUG
    }
    public function getCurrentTemperature() {
      return $this->currentTemperature;
    }
    public function setCurrentTemperature($degreesCelsius) {
      $this->currentTemperature = max(-50, min(100, $degreesCelsius));
      $_SESSION['currentTemperature'] = $this->getCurrentTemperature();
      // echo "(DEBUG setter) Current temperature is:     ".$this->currentTemperature."<br>"; // DEBUG
      // echo "(DEBUG setter) Session temperature var is: ".$_SESSION['currentTemperature']."<br>"; //DEBUG
    }
    public function raiseTemperature($degreesCelsius) {
      $tmpTemp = $this->getCurrentTemperature() + $degreesCelsius;
      $this->setCurrentTemperature($tmpTemp);
    }
    public function lowerTemperature ($degreesCelsius) {
      $tmpTemp = $this->getCurrentTemperature() - $degreesCelsius;
      $this->setCurrentTemperature($tmpTemp);
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title></title>
  </head>
  <body>
    <?php
      if (!isset($_SESSION['currentTemperature'])) {
        $_SESSION['currentTemperature'] = 25;
      }
      $thermometer = new Thermometer($_SESSION['currentTemperature']);
      echo "<p>De huidige temperatuur bedraagt: ";
      echo $thermometer->getCurrentTemperature();
      echo " graden Celsius</p>";
    ?>
    <form action='8-3.php' method='post'>
      Wat is het temperatuurverschil (in graden Celsius)? <input type='number' name='tempDiff'><br>
      Is het warmer of kouder?
      <input type='radio' name='upDown' value='warmer'> warmer
      <input type='radio' name='upDown' value='colder'> kouder
      <input type='submit' value='Submit' name='tempChange'>
    </form>
    <br>
    <?php
      if (isset($_POST['tempChange'])) {
        switch ($_POST['upDown']) {
          case 'warmer' : $thermometer->raiseTemperature($_POST['tempDiff']);
                          break;
          case 'colder' : $thermometer->lowerTemperature($_POST['tempDiff']);
                          break;
        }
      }
      $newTemperature = $thermometer->getCurrentTemperature();
      echo "De huidige (nieuwe) temperatuur bedraagt: ";
      echo $thermometer->getCurrentTemperature();
      echo " graden Celsius<br>";      
    ?>
  </body>
</html>
