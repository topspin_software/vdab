<?php
//fibonacci
  class Fibonacci {
    public function generateSequence($max) {
      $fibonacci[] = 0;
      $fibonacci[] = $i = 1;
      do {
        $i++;
        $fibonacci[$i] = $fibonacci[$i-1] + $fibonacci[$i-2];
      } while ($fibonacci[$i] < $max);
      unset($fibonacci[$i]);
      return $fibonacci;
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <pre>
    <?php
      $sequence = new Fibonacci;
      $fibo_reeks = $sequence->generateSequence(5000);
      print_r($fibo_reeks);
      //quick&dirty test: change for-loop counter inside loop
      for ($i=0;$i<1000;$i++) {
        if (($i%3) == 0) {
          $i++; }
        print $i. " ";
      }
    ?>
    </pre>
  </body>
</html>
