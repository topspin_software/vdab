<?php
  class RandomNumbers {
    public function generateNumbers($count, $min, $max) {
      for ($i=0; $i<$count; $i++) {
        $sequence[] = mt_rand($min, $max);
      }
      return $sequence;
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <pre>
      <?php
      $randomreeks = new RandomNumbers;
      print "een reeks van 20 willekeurige getallen tussen -50 en 50:" . PHP_EOL;
      print_r($randomreeks->generateNumbers(20, -50, 50));
      ?>
    </pre>
  </body>
</html>
