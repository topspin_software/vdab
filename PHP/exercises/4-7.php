<?php
  class RandomReeks {
    public function getRandomReeks($aantal, $min, $max) {
      $reeks=array();
      //initialize array
      for ($i=$min; $i<=$max; $i++) { $reeks[$i] = 0; }
      //generate random value and add 1 to corresponding array key
      for ($i=0; $i<$aantal; $i++) {
        $key = intval(mt_rand($min, $max));
        $reeks[$key] += 1;
      }
    return $reeks;  
    }
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title></title>
  </head>
  <body>
    <form action='4-7.php' method='post'>
      Er zullen 
      <input type='number' name='aantal'> getallen gegenereerd worden tussen 
      <input type='number' name='minimum'> en 
      <input type='number' name='maximum'>.
      <input type='submit' value='OK'>
    </form>
    <pre>
      <?php
      // initialize vars to avoid errors when waiting for form input
      if (isset($_POST['aantal'])) {
        $Aantal = $_POST['aantal'];
        $Min = $_POST['minimum'];
        $Max = $_POST['maximum'];
        $Frequentie = new RandomReeks;
        $frequentie = $Frequentie->getRandomReeks($Aantal, $Min, $Max);
        print PHP_EOL;
        for ($t=$Min; $t<=$Max; $t++) {
          echo 'Getal ' . $t . ' werd ' . $frequentie[$t] . ' keer gegenereerd.';
          print PHP_EOL;
        }
      }
      ?>
    </pre>
  </body>
</html>
