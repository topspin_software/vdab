<!DOCTYPE html>

<html>
  <head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, height=device-height, initial-scale=1.0'/>
    <meta name='author' content='Sven Croon'/>
    <meta name='description' content='PHP oefeningen - landing page'/>
    <title>PHP exercises</title>
    <!-- [if lt IE 9]><script src="bower_components/html5shiv/dist/html5shiv.js"></script><![endif] -->
    <link href='css/exercises.css' type='text/css' rel='stylesheet'/>
  </head>
  <body>
    <nav>
      <ul>
        <li>hoofdstuk 1
          <ul>
            <li><a href='helloworld.php'>Hello World</a></li>            
            <li><a href='rekenmachine.php'>rekenmachine</a></li>            
            <li><a href='1-1-rekenmachine.php'>oefening 1.1</a></li>
            <li><a href='1-2-rekenmachine.php'>oefening 1.2</a></li>
            <li><a href='1-3-rekenmachine.php'>oefening 1.3</a></li>
          </ul>
        </li>
        <li>hoofdstuk 2
          <ul>
            <li><a href='geheim.php'>geheim</a></li>
            <li><a href='2-1.txt'>oefening 2.1</a></li>
            <li><a href='2-2.php'>oefening 2.2</a></li>            
            <li><a href='2-3.php'>oefening 2.3</a></li>
            <li><a href='2-5.php'>oefening 2.5</a></li>
            <li><a href='2-7.php'>oefening 2.7</a></li>
            <li><a href='2-8.php'>oefening 2.8</a></li>
            <li><a href='2-9.php'>oefening 2.9</a></li>
            <li><a href='2-10.php'>oefening 2.10</a></li>
            <li><a href='2-11.php'>oefening 2.11</a></li>
            <li><a href='2-12.php'>oefening 2.12</a></li>
            <li><a href='2-13.php'>oefening 2.13</a></li>
          </ul>
        </li>
        <li>hoofdstuk 3
          <ul>
            <li><a href='3-GET.php'>hoofdstuk 3 - GET</a></li>
            <li><a href='3-POST.php'>hoofdstuk 3 - POST</a></li>
            <li><a href='3-1.php'>oefening 3.1</a></li>
            <li><a href='3-2.php'>oefening 3.2</a></li>      
            <li><a href='3-3.php'>oefening 3.3</a></li>
            <li><a href='3-4.php'>oefening 3.4</a></li>
            <li><a href='3-5.php'>oefening 3.5</a></li>      
            <li><a href='3-6.php'>oefening 3.6</a></li>   
          </ul>
        </li>
        <li>hoofdstuk 4
          <ul>
            <li><a href='4-1.php'>oefening 4.1</a></li>
            <li><a href='4-2.php'>oefening 4.2</a></li>
            <li><a href='4-3.php'>oefening 4.3</a></li>      
            <li><a href='4-4.php'>oefening 4.4</a></li>
            <li><a href='4-5.php'>oefening 4.5</a></li>
            <li><a href='4-6.php'>oefening 4.6</a></li>      
            <li><a href='4-7.php'>oefening 4.7</a></li>   
          </ul>
        </li>
        <li>hoofdstuk 5
          <ul>
            <li><a href='5-1.php'>oefening 5.1</a></li>
            <li><a href='5-2.php'>oefening 5.2</a></li>
            <li><a href='5-3.php'>oefening 5.3</a></li>      
            <li><a href='5-4.php'>oefening 5.4</a></li>
            <li><a href='5-5.php'>oefening 5.5</a></li>
          </ul>
        </li>
        <li>hoofdstuk 6
          <ul>
            <li><a href='6-1.php'>oefening 6.1</a></li>
            <li><a href='6-1-extra.php'>oefening 6.1 - extra</a></li>            
            <li><a href='6-2.php'>oefening 6.2</a></li>
          </ul>
        </li>
        <li>hoofdstuk 7 - herhalingsoefeningen
          <ul>
            <li><a href='7-1.php'>oefening 7.1</a></li>
            <li><a href='7-2-tafelsForm.php'>oefening 7.2</a></li>            
            <li><a href='7-3.php'>oefening 7.3</a></li>
            <li><a href='7-4.php'>oefening 7.4</a></li>
            <li><a href='7-5.php'>oefening 7.5</a></li>            
            <li><a href='7-6.php'>oefening 7.6</a></li>
          </ul>
        </li>
        <li>hoofdstuk 8 - object-oriëntatie
          <ul>
            <li><a href='8-1.php'>oefening 8.1</a></li>
            <li><a href='8-2.php'>oefening 8.2</a></li>            
            <li><a href='8-3.php'>oefening 8.3</a></li>
            <li><a href='8-4.php'>oefening 8.4</a></li>
            <li><a href='8-5.php'>oefening 8.5</a></li>            
            <li><a href='8-6.php'>oefening 8.6</a></li>
            <li><a href='8-7.php'>oefening 8.7</a></li>            
            <li><a href='8-8.php'>oefening 8.8</a></li>
          </ul>
        </li>
        <li>hoofdstuk 9 - database toegang
          <ul>
            <li><a href='9-1.php'>oefening 9.1</a></li>
            <li><a href='9-2.php'>oefening 9.2</a></li>            
            <li><a href='9-3.php'>oefening 9.3</a></li>
            <li><a href='9-4.php'>oefening 9.4</a></li>
            <li><a href='9-5.php'>oefening 9.5</a></li>            
            <li><a href='9-6.php'>oefening 9.6</a></li>
            <li><a href='9-7.php'>oefening 9.7</a></li>            
            <li><a href='9-8.php'>oefening 9.8</a></li>
          </ul>
        </li>        
      </ul>
    </nav>
    <?php
    // put your code here
    ?>
  </body>
</html>
