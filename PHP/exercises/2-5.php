<?php
//vergelijking.php

class Vergelijking {
  public function getSomIsStriktPositief ($getal1, $getal2) {
    $antw = (($getal1 + $getal2) > 0);
    if ($antw == true) {
      return "JA";
    }
    else {
      return "NEE";
    }
  }
  public function getSomIsStrictNegatief ($getal1, $getal2, $getal3) {
    $antw = (($getal1 + $getal2 + $getal3) < 0);
    if ($antw == true) {
      return "JA";
    }
      else {
      return "NEE";
    }
  }
}
?>

<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8">
    <title>oefening 2.5 - Vergelijking</title>
  </head>

  <body>
    <?php
    $vgl = new Vergelijking;
    print ($vgl->getSomIsStriktPositief(10, -9));
    echo ("\r\n");
    print ($vgl->getSomIsStrictNegatief(15, -12, -8));
    ?>
  </body>

</html>
