<?php
  class Person {
    private $familyName;
    private $firstName;
    public function __construct($par_familyName, $par_firstName) {
      $this->setFamilyName($par_familyName);
      $this->setFirstName($par_firstName);
    }
    public function getFirstName() {
      return $this->firstName;
    }
    public function getFamilyName() {
      return $this->familyName;
    }
    public function getFullName() {
      return $this->getFamilyName().", ".$this->getFirstName();
    }
    public function setFirstName($par_firstName) {
      $this->firstName = $par_firstName;
    }
    public function setFamilyName($par_familyName) {
      $this->familyName = $par_familyName;
    }
  }
  class Student extends Person {
    private $numberOfCourses;
    public function __construct($par_familyName, $par_firstName, $par_numberOfCourses) {
      parent::__construct($par_familyName, $par_firstName);
      $this->setNumberOfCourses($par_numberOfCourses);
    }
    public function setNumberOfCourses($par_numberOfCourses) {
      $this->numberOfCourses = $par_numberOfCourses;
    }
    public function getFullName() {
      return parent::getFullName()." (student)";
    }
    public function getNumberOfCourses() {
      return $this->numberOfCourses;
    }
  }
  class Assistant extends Person {
    private $numberOfStudents;
    public function __construct($par_familyName, $par_firstName, $par_numberOfStudents) {
      parent::__construct($par_familyName, $par_firstName);
      $this->setNumberOfStudents($par_numberOfStudents);
    }
    public function setNumberOfStudents($par_numberOfStudents) {
      $this->numberOfStudents = $par_numberOfStudents;
    }
    public function getFullName() {
      return parent::getFullName()." (assistant)";
    }
    public function getNumberOfStudents() {
      return $this->numberOfStudents;
    }
  }
	$student = new Student('Peeters', 'Jan', 3);
	$assistant = new Assistant('Janssens', 'Tom', 8);
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>oef 8.5 - Students and Assistants</title>
  </head>
  <body>
  	<h1>Names</h1>
		<ul>
			<?php
        $tmp_courses = $student->getNumberOfCourses();
        switch ($tmp_courses) {
          case 1  : $str_courses = 'cursus';
                    break;
          default : $str_courses = 'cursussen';
                    break;
        }
        echo '<li>'.$student->getFullName().' volgt '.$tmp_courses.' '.$str_courses.'</li>';
        $tmp_students = $assistant->getNumberOfStudents();
        switch ($tmp_students) {
          case 1  : $str_students = 'student';
                    break;
          default : $str_students = 'studenten';
                    break;
        }
			  echo '<li>'.$assistant->getFullName().' begeleidt '.$tmp_students.' '.$str_students.'</li>';
      ?>
		</ul>
  </body>
</html>
