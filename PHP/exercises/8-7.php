<?php
abstract class Account {
  private $accountNumber;
  private $balance;
  public function __construct($par_accountNumber) {
    $this->setAccountNumber($par_accountNumber);
    $this->balance = 0;
  }
  public function setAccountNumber($par_accountNumber) {
    $this->accountNumber = $par_accountNumber;
  }
  public function getAccountnNumber() {
    return $this->accountNumber;
  }
  public function setBalance($par_balance) {
    $this->balance = $par_balance;
  }
  public function getBalance() {
    return $this->balance;
  }
  public function creditAccount($amount) {
    if ($amount < 0) {
      echo "You cannot credit this account with a negative amount! Request cancelled.";
      echo "Please specify only positive amounts.";
      return -1;
    } 
    $tmp_amount = $this->getBalance() + $amount;
    $this->setBalance($tmp_amount);
  }
  public function debetAccount($amount) {
    if ($amount < 0) {
      echo "You cannot debet this account with a negative amount! Request cancelled.";
      echo "Please specify only positive amounts.";
      return -1;
    } 
    $tmp_amount = $this->getBalance() - $amount;
    $this->setBalance($tmp_amount);
  }
  public function processInterest() {
    $tmp_balance = $this->getBalance() * (1 + $this->getInterest());
    $this->setBalance($tmp_balance);
  }    
}
class SavingsAccount extends Account {
  private static $interest = 0.03;
  public static function setInterest($par_interest) {
    self::$interest = $par_interest;
  }
  public static function getInterest() {
    return self::$interest;
  }
}
class CurrentAccount extends Account {
  private static $interest = 0.025;
  public static function setInterest($par_interest) {
    self::$interest = $par_interest;
  }
  public static function getInterest() {
    return self::$interest;
  }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title></title>
  </head>
  <body>
    <section>
      <h2>Current Account</h2>
      <?php
			$currentAccount = new CurrentAccount("091-0122401-16");
			print("Het saldo is: " .$currentAccount->getBalance() . "<br />");
			$currentAccount->creditAccount(200);
			print("Het saldo is: " .$currentAccount->getBalance() . "<br />");
			$currentAccount->processInterest();
			print("Het saldo is: " .$currentAccount->getBalance() . "<br />");
      echo "<br/>";
      ?>
      <h2>Savings Account</h2>
      <?php
			$savingsAccount = new SavingsAccount("091-0122401-16");
			print("Het saldo is: " .$savingsAccount->getBalance() . "<br />");
			$savingsAccount->creditAccount(200);
			print("Het saldo is: " .$savingsAccount->getBalance() . "<br />");
			$savingsAccount->processInterest();
			print("Het saldo is: " .$savingsAccount->getBalance() . "<br />");
			?>
		</section>
  </body>
</html>
