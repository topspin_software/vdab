<?php
//class related code
class Movie
{
    private static $dsn = 'mysql:host=localhost;dbname=cursusphp;charset=utf8';
    private static $usr = 'cursusgebruiker';
    private static $pwd = 'cursuspwd';
    private static $dbh = null;
    private $movieID = null;
    private $movieTitle = null;
    private $movieRuntime = null;
    public function __construct($title = null, $runtime = null)
    {
        $this->setMovieTitle($title);
        $this->setMovieRuntime($runtime);
    }
    private static function connectDB()
    {
        self::$dbh = new PDO(self::$dsn, self::$usr, self::$pwd);
    }
    private static function disconnectDB()
    {
        self::$dbh = null;
    }
    public function setMovieID($id)
    {
        $this->movieID = $id;
    }
    public function setMovieTitle($title)
    {
        $this->movieTitle = $title;
    }
    public function setMovieRuntime($runtime)
    {
        $this->movieRuntime = $runtime;
    }
    public static function getMovieList()
    {
        self::connectDB();
        $resultSet = self::$dbh->query('SELECT * FROM films');
        self::disconnectDB();
        return $resultSet;
    }
    public function addMovie()
    {
        self::connectDB();
        $sql = 'INSERT INTO films (titel, duurtijd) VALUES (:title, :runtime)';
        $stmt = self::$dbh->prepare($sql);
        $stmt->bindParam(':title', $this->movieTitle);
        $stmt->bindParam(':runtime', $this->movieRuntime);
        $bool = $stmt->execute();
        if ($bool == FALSE) { echo 'execute FAILED'; }
        $this->movieID = self::$dbh->lastInsertId();
        $stmt = null;
        self::disconnectDB();
    }
    public function updateMovie()
    {
        self::connectDB();
        $sql = 'UPDATE films SET titel = :title, duurtijd = :runtime WHERE id = :id';
        $stmt = self::$dbh->prepare($sql);
        $stmt->bindParam(':title', $this->movieTitle);
        $stmt->bindParam(':runtime', $this->movieRuntime);
        $stmt->bindParam(':id', $this->movieID);
        $bool = $stmt->execute();
        if ($bool == FALSE) { echo 'execute FAILED'; }
        $stmt = null;
        self::disconnectDB();
    }
    public static function eraseMovie($id)
    {
        self::connectDB();
        $sql = 'DELETE FROM films WHERE id = :id';
        $stmt = self::$dbh->prepare($sql);
        $stmt->bindParam(':id', $id);
        $bool = $stmt->execute();
        if ($bool == FALSE) { echo 'execute FAILED'; }
        $stmt = null; 
        self::disconnectDB();    
    }
}
//generic code
session_start();
if (isset($_GET['action']))
{
    switch ($_GET['action']) {
    case 'verwijder' :
        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
        Movie::eraseMovie($id);
        break;
    case 'edit' :
        $id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
        $_SESSION['edit'] = $id;
        break;
    case 'post_submitted' :
        $newTitle = filter_input(INPUT_POST, 'edit_title', FILTER_SANITIZE_STRING);
        $newRuntime = filter_input(INPUT_POST, 'edit_runtime', FILTER_SANITIZE_NUMBER_INT);    
        $movie = new Movie($newTitle, $newRuntime);
        $movie->setMovieID($_SESSION['edit']);
        $movie->updateMovie();
        unset($_SESSION['edit']);
        unset($_POST);
        break;
    case 'toevoegen' :
        $title = filter_input(INPUT_POST, 'title', FILTER_SANITIZE_STRING);
        $runtime = abs(filter_input(INPUT_POST, 'runtime', FILTER_VALIDATE_INT));
        $newMovie = new Movie($title, $runtime);
        $newMovie->addMovie();
        break;
    default :
        break;
    }
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset='UTF-8'>
    <title>Film List</title>
    <link rel='stylesheet' type='text/css' href='css/exercises.css'/>
  </head>
  <body>
    <div class='formborder'>
      <h2>Alle Films</h2>
      <?php
      $movieList = Movie::getMovieList();
      echo '<lu class=movielist>';
      foreach ($movieList as $row)
      {
          echo '<li><a href="9-4.php?action=verwijder&id=';
          echo $row['id'];
          echo '"><img src=images/delete.png></a> ';
          if (isset($_SESSION['edit']) && $_SESSION['edit'] == $row['id'])
          {
              //show form for this movie - edit movie info
              echo '<form class="inlineForm" action="9-4.php?action=post_submitted" method="post">';
              echo '<input type="text" name="edit_title" value="'.$row['titel'].'"> ';
              echo '<input type="text" name="edit_runtime" value="'.$row['duurtijd'].' min"> <input type="submit" value="OK">';
              echo '</form>';
          } else {
              // show movie title and runtime info (clickable link)
              echo '<a class="nonformattedlink" href="9-4.php?action=edit&id=';
              echo $row['id'];
              echo '">'.$row['titel'].' ('.$row['duurtijd'].' min)</a></li>';              
          }
      }
      echo '</lu>';   
      ?>
    </div>
    <div class='formborder'>
      <h2>Film toevoegen</h2>
        <form action='9-4.php?action=toevoegen' method='post'>
          Titel: <input type='text' name='title' autocomplete='off' required>
          Duurtijd: <input type='number' name='runtime'>
          <input type='submit' value='OK'>
      </form>
    </div>      
  </body>
</html>
