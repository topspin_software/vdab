<?php
//switch
class GeheelGetal {
  public function getVoluit($getal) {
    switch ($getal) {
      case 1: return "een";
      case 2: return "twee";
      case 3: return "drie";
      case 4: return "vier";
      case 5: return "vijf";
      default: return $getal;
    }
  }
}
?>
<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8">
    <title>oefening 2.9 - switch</title>
  </head>

  <body>
    <h1>
      <?php
      $number = new GeheelGetal;
      print ($number->getVoluit(1));
      ?>
    </h1>
  </body>

</html>
