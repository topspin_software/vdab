<?php
// 9-7-login.php -- show and process login screen
// included in 9-7.php

require '9-7-dao.php';

$loggedIn = isset($_GET['login']) ? filter_input(INPUT_GET, 'login', FILTER_VALIDATE_BOOLEAN) : FALSE;
if ($loggedIn) {
    $usr = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $pwd = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $loginResult = DAO::checkLogin($usr,$pwd);
    if ($loginResult[0]) {
        session_start();
        header ('refresh: 5; url=9-7-logged_in.php');
    }
    echo $loginResult[1];
}
?>

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset='UTF-8'>
        <title>Four-In-A-Row</title>
        <link rel='stylesheet' type='text/css' href='css/exercises.css'/>        
    </head>
    <body>
        <h1 class='four_in_a_row_h1'>Four-In-A-Row</h1>
        <form class='four_in_a_row_login' action='9-7-login.php?login=TRUE' method='post'>
            <label class='formLabel' for='usr'>Username</label
            ><input class='formInput' type='text' name='username' id='usr' placeholder='username' align='right'>
            <br/>
            <label class='formLabel' for='pwd'>Password</label
            ><input class='formInput' type='password' name='password' size='20' id='pwd' placeholder='password' align='right'>
            <br/>
            <input class='formSubmit' type='submit' value='Log in'>
        </form> 
    </body>
</html>

